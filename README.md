# PodcastOne Firebase Cloud Functions

This project contains source code and supporting files for PodcastOne Project.  
Firebase Cloud functions - Smart Speaker application and Comment Email Notification.
Firebase Hosting - Login UI for Alexa and Google Assistant Account Linking.
Firebase Storage - Reference slug and synonyms used for Dialogflow Entities and Alexa Slots under the filename podcast_entries_en.json.

The repo enables you to deploy these functions, hosting and firestore with the Firebase CLI. It includes the following files and folders.

- functions/dialogflowFirebaseFulfillment - Dialogflow Google Assistant fulfillment code.
- functions/access_token - OAuth2 Code Token Exchange for Alexa and Google Assistant Account Linking (Combine And ClientID Header Authorization)
- functions/commentsEmailNotification - Email notification for new and reported episode comments.
- functions/apinewpodcast - API NewPodcast for updating Alexa Slot and Dialogflow
- functions/test - Dialogflow Unit Test and Integration Test
- dist/alexalink - Login UI for Alexa Account Linking
- dist/ghomelink - Login UI for Google Assistant Account Linking
- firestore.rules - Firestore Read/Write rules config
- gitlab-ci.yml - CI/CD using Gitlab

The application uses Firebase resources in Google Cloud Platform. These resources are defined in the `firebase.json` file in this project. You can update the template to add Firebase resources through the same deployment process that updates your application code.

## Install Firebase CLI and Sign In

To use the Firebase CLI, you need the following tools.

* Firebase CLI - [Install the Firebase CLI](https://firebase.google.com/docs/cli) 
* Node.js - [Install Node.js 8](https://nodejs.org/en/), including the NPM package management tool.

Sign into Firebase using your Google account by running the following command:

```bash
firebase login
```

## Project alias
These are the current project environments.

| Environment | Name | Project ID / Instance |
| --- | --- | --- |
| `default` | TEST - PodcastOne | test-podcastone | 
| `prod` | PodcastOne Australia | podcastone-australia | 
| `uat` | PodcastOne Australia UAT | podcastone-australia-de2a3 | 

To list projects:

```bash
firebase use
```

To switch to project (e.g. production):

```bash
firebase use prod
```

## Smart Speaker Architecture

![GA-SmartSpeaker img](files/GA-SmartSpeaker.png)

| No | Tech Stack | Link |
| --- | --- | --- |
| `1` | Dialogflow | [DEV](https://dialogflow.cloud.google.com/#/agent/03f85207-7d1c-4e67-a2fc-85d77d50a934/intents) [UAT](https://dialogflow.cloud.google.com/#/agent/5c8758ec-9d3b-4235-b29e-df4e7fd456c0/intents) [PROD](https://dialogflow.cloud.google.com/#/agent/b95e8312-2aaf-49b1-8694-64f57997e9f7/intents) | 
| `2` | Firebase | [DEV](https://console.firebase.google.com/u/0/project/test-podcastone/overview) [UAT/PROD](https://console.firebase.google.com/u/0/project/podcastone-australia/overview) | 
| `3` | Actions | [DEV](https://console.actions.google.com/u/0/project/test-podcastone/release/) [UAT](https://console.actions.google.com/u/0/project/podcastone-australia-de2a3/release/) [PROD](https://console.actions.google.com/u/0/project/podcastone-australia/release/) | 
| `4` | Stackdriver | [DEV](https://console.cloud.google.com/logs/viewer?project=test-podcastone&folder&organizationId&minLogLevel=0&expandAll=false&customFacets=&limitCustomFacetWidth=true&interval=P1D&resource=cloud_function%2Ffunction_name%2FdialogflowFirebaseFulfillment%2Fregion%2Fus-central1&filters=text:queryTextPlay) [UAT/PROD](https://console.cloud.google.com/logs/viewer?project=podcastone-australia&folder&organizationId&minLogLevel=0&expandAll=false&customFacets=&limitCustomFacetWidth=true&interval=P1D&resource=cloud_function%2Ffunction_name%2FdialogflowFirebaseFulfillment%2Fregion%2Fus-central1&filters=text:queryTextPlay) | 


## Comment Notification

#### Change email recipient
To change the email recipient for each project you need to set the Firebase environment configuration:
1. Switch to the intended project (See Project alias).
2. To list the current environment configuration
    ```bash
    firebase functions:config:get
   ```
3. To set the email recipient
    ```bash
    firebase functions:config:set sendgrid.emailto="Your email recipient"
   ```
   
#### Set SendGrid API key
  ```bash
    firebase functions:config:set sendgrid.key="API KEY"
   ```

## API New Podcast

#### Change administrator email recipient
To change the administrator email recipient for each project you need to set the Firebase environment configuration:
1. Switch to the intended project (See Project alias).
2. To list the current environment configuration
    ```bash
    firebase functions:config:get
   ```
3. To set the email recipient
    ```bash
    firebase functions:config:set sendgrid.newpodcast="AdminEmail"
   ```

## Deploy Hosting Application

To deploy the application (Login UI for Account linking), use this:

```bash
firebase deploy --only hosting
```

## Deploy specific functions

When deploying functions, you can target specific functions. For example:

```bash
firebase deploy -f --only functions:dialogflowFirebaseFulfillment
```

## Unit and Integration Tests

Tests are defined in the `function/tests` folder in this project. Use NPM to install the [Mocha framework](https://mochajs.org/), [Chai framework](https://www.chaijs.com/), [AVA framework](https://avajs.dev/), and run unit tests.

|  Test file | Description |
| --- | --- |
| `df-test` | Unit Test for Dialogflow |
| `index-test` | Unit Test for Dialogflow Google Integration Fulfillment |
| `integration-test` | Integration Test for Dialogflow |

To start testing, use this:

```bash
cd functions
npm install
npm test
```
