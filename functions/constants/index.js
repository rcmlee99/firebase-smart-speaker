function define(name, value) {
    Object.defineProperty(exports, name, {
        value:      value,
        enumerable: true
    });
}

define("podcastListLimit", 3);
define("audioLink", "https://ghome-oauth-link.firebaseapp.com/sound/podcastone.mp3");
define("cardImageUrl", "https://ghome-oauth-link.firebaseapp.com/img/PC1_512x512.png");
define("TAGLINE", "Australia's premium podcast network");
define("SKILL_NAME", "Podcast One Australia");
define("portUserServiceManager", 3000);
define("portAPIFirestore", 3001);
define("portAPINewPodcast", 3002);