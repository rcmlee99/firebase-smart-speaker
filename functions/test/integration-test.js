// This file contains integration tests for a Dialogflow agent
// and fulfillment code.
'use strict';
const test = require('ava');
const {expect} = require('chai');
const {DialogflowApiFactory} = require('./lib/df-api.js');
const uuid = require('uuid');

/* ====== Substitute the following variables (START) ===== */
const projectId = 'test-podcastone';
// Make sure the service account has "Dialogflow API Client" in GCP IAM
const pathToServiceAccount = './key.json';
// The value itself doesn't matter (can be anything).
const sessionId = uuid.v1();
/* ====== Substitute the following variables (END) ======= */

const serviceAccount = require(pathToServiceAccount);
let dialogflow = undefined;

test.before(async function(t) {
  dialogflow = await DialogflowApiFactory.create({
    projectId: projectId,
    serviceAccount: serviceAccount,
    sessionId: sessionId,
  });
});

test.afterEach(async function(t) {
  await dialogflow.clearSession(sessionId);
});

/*
This test asserts various properties about the response
returned from your fulfillment when Dialogflow receives
"Play Podcast" query. Particularly, test checks
that fulfillment code correctly set rich response.

In other words, the test checks
that integration between Dialogflow intent matching & entity
resolution and your fulfillment works correctly.

There is no device surface, hence the response will only be simpleResponse.
*/
test.serial('Play Hamish Andy', async function(t) {
  const jsonRes = await dialogflow.detectIntent(
    'Play podcast'
  );
  const payload = jsonRes.queryResult.webhookPayload;
  expect(payload).to.have.deep.keys('google');
  expect(payload.google.expectUserResponse).to.be.false;
  expect(payload.google.richResponse.items[0]).to.have.deep.keys('simpleResponse');
  t.pass();
});

/*
This test asserts various properties about the response
returned from your fulfillment when Dialogflow receives
"Next Podcast" query. Particularly, test checks
that fulfillment code correctly set rich response.

In other words, the test checks
that integration between Dialogflow intent matching & entity
resolution and your fulfillment works correctly.

There is no device surface, hence the response will only be simpleResponse.
*/
test.serial('Next episode', async function(t) {
  const jsonRes = await dialogflow.detectIntent(
    'Next podcast'
  );
  const payload = jsonRes.queryResult.webhookPayload;
  expect(payload).to.have.deep.keys('google');
  expect(payload.google.expectUserResponse).to.be.false;
  expect(payload.google.richResponse.items[0]).to.have.deep.keys('simpleResponse');
  t.pass();
});