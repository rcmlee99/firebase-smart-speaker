// This file contains unit tests for a GraphQL service.

const test = require('ava');
const { expect } = require('chai');
const { getRecommendedList, getPodcastEpisodesById, getEpisodeDetail, getPodcastEpisodes } = require('../service/graphqlService');
const { findEntities } = require('../service/dialogflowService');
const { initialiseFirebaseAdmin, getUserLastEpisode, getUserFavShows, addOrUpdateUserEpisode, addOrUpdateUserFavouritePodcast, getBucket } = require('../service/firebaseAdminService');
const { mockFirestoreData } = require('./static/mockData');
const Ajv = require('ajv');
const ajv = new Ajv({logger: console});

// Initialise FirebaseAdmin before using firebase functions
initialiseFirebaseAdmin();

test.serial('GraphQLService getRecommendedList', async (t) => {
    const result = await getRecommendedList();
    expect(result).to.be.an('object');
    expect(result.popularPodcasts).to.be.an("array");
    t.pass();
});

test.serial('GraphQLService getPodcastEpisodesBySlug', async (t) => {
    const result = await getPodcastEpisodes("crappy-to-happy");
    expect(result).to.be.an('object');
    expect(result.podcast).to.be.an("object");
    expect(result.podcast.name).to.be.an("string");
    expect(result.podcast.episodes).to.be.an("object");
    expect(result.podcast.episodes.items).to.be.an("array");
    t.pass();
});

test.serial('GraphQLService getPodcastEpisodesById', async (t) => {
    // crappy to happy 1492d75c-395a-4c9c-94f1-aa4c003fa01a
    const result = await getPodcastEpisodesById("1492d75c-395a-4c9c-94f1-aa4c003fa01a");
    expect(result).to.be.an('object');
    expect(result.podcast).to.be.an("object");
    expect(result.podcast.slug).to.be.an("string");
    expect(result.podcast.name).to.be.an("string");
    expect(result.podcast.episodes).to.be.an("object");
    expect(result.podcast.episodes.items).to.be.an("array");
    t.pass();
});

test.serial('GraphQLService getEpisodeDetail', async (t) => {
    // just-the-gist episode 4a4d9663-86b8-496b-9781-ab60016bc468
    const result = await getEpisodeDetail("4a4d9663-86b8-496b-9781-ab60016bc468");
    expect(result.episode).to.be.an('object');
    expect(result.episode.title).to.be.an('string');
    expect(result.episode.slug).to.be.an('string');
    expect(result.episode.smallImageUrl).to.be.an('string');
    expect(result.episode.audioUrl).to.be.an('string');
    expect(result.episode.podcast).to.be.an('object');
    expect(result.episode.podcast.name).to.be.an('string');
    expect(result.episode.podcast.slug).to.be.an('string');
    t.pass();
});

test.serial('FirebaseAdminService getUserLastEpisode', async (t) => {
    const result = await getUserLastEpisode(mockFirestoreData);
    expect(result).to.be.an('object');
    expect(result).to.have.all.keys('episodeId', 'playHeadPosition', 'playedDateTime', 'duration', 'isMarkedAsPlayed');
    t.pass();
});

test.serial('FirebaseAdminService getUserFavShows', async (t) => {
    const showSchema = {
        "type": "array",
        "items": {
          "type": "string",
        }
      };
    const result = await getUserFavShows(mockFirestoreData);
    expect(result).to.be.an('array');
    expect(ajv.validate(showSchema, result)).to.be.true;
    t.pass();
});

test.serial('FirebaseAdminService addOrUpdateUserEpisode', async (t) => {
    const result = await addOrUpdateUserEpisode(mockFirestoreData);
    expect(result).to.be.an('object');
    expect(result._writeTime).to.have.all.keys('_seconds', '_nanoseconds');
    t.pass();
});

test.serial('FirebaseAdminService addOrUpdateUserFavouritePodcast', async (t) => {
    const result = await addOrUpdateUserFavouritePodcast(mockFirestoreData);
    expect(result).to.be.an('object');
    expect(result._writeTime).to.have.all.keys('_seconds', '_nanoseconds');
    t.pass();
});

const entriesSchema = {
    "type": "array",
    "items": {
      "type": "object",
      "properties": {
        "value": {
          "type": "string"
        },
        "synonyms": {
          "type": "array",
          "items" : {
            "type": "string",
          }
        }
      }
    }
  };

test.serial('FirebaseAdminService getBucket', async (t) => {
    const result = await getBucket('podcast_entries_en.json');
    expect(result).to.be.an('array');
    expect(ajv.validate(entriesSchema, result)).to.be.true;
    t.pass();
});

test.serial('DialogflowService findEntities', async (t) => {
    const result = await findEntities('podcast');
    expect(result).to.be.an('object');
    expect(result.entities).to.be.an('array');
    expect(ajv.validate(entriesSchema, result.entities)).to.be.true;
    t.pass();
});

