// This file contains unit tests for the fulfillment.
'use strict';

const {expect} = require('chai');
const fs = require('fs');
const path = require('path');

const { dialogflowtest } = require('../dialogflowFirebaseFulfillment.js');

const testApp = dialogflowtest;
const test = require('ava');

/**
 * Calls the DialogflowApp or ActionsSDKApp instance.
 * Reads the request from disk in the file named titleOfStaticJson.
 * @param {string} titleOfStaticJson
 * @return {Object} webhook response
 */
async function getAppResponse(titleOfStaticJson) {
  // load the static json at runtime
  const staticJson = JSON.parse(
    fs.readFileSync(
      // this will look for a file that has the basename matching
      // the name of the it-block. This is done to simplify code,
      // but not neccessary. If you change this line to something
      // else make sure it matches name of the JSON file.

      // eslint-disable-next-line no-invalid-this
      path.join(__dirname, 'static', titleOfStaticJson + '.json')
    )
  );
  let jsonRes = await testApp(staticJson, {}); // 2nd param is the header
  console.log("::::: TestApp :::::::");
  return jsonRes.body;
}

/*
This test asserts various properties about the response received from
triggering your fulfillment with test/static/carousel.json. Particularly,
it checks that fulfillment code correctly set rich response.

*/
 test.serial('carousel', async function(t) {
  const jsonRes = await getAppResponse('carousel');
  expect(jsonRes.payload).to.have.deep.keys('google');
  expect(jsonRes.payload.google.expectUserResponse).to.be.true;
  expect(jsonRes.payload.google.richResponse.items).to.have.lengthOf(2);
  expect(jsonRes.payload.google.richResponse.items[1].mediaResponse).to.include({"mediaType":"AUDIO"});
  t.pass();
});

test.serial('playpodcast', async function(t) {
  const jsonRes = await getAppResponse('playpodcast');
  expect(jsonRes.payload).to.have.deep.keys('google');
  expect(jsonRes.payload.google.expectUserResponse).to.be.true;
  expect(jsonRes.payload.google.richResponse.items).to.have.lengthOf(2);
  expect(jsonRes.payload.google.richResponse.items[1].mediaResponse).to.include({"mediaType":"AUDIO"});
  t.pass();
});

test.serial('nextepisode', async function(t) {
  const jsonRes = await getAppResponse('nextepisode');
  expect(jsonRes.payload).to.have.deep.keys('google');
  expect(jsonRes.payload.google.expectUserResponse).to.be.true;
  expect(jsonRes.payload.google.richResponse.items).to.have.lengthOf(2);
  expect(jsonRes.payload.google.richResponse.items[1].mediaResponse).to.include({"mediaType":"AUDIO"});
  t.pass();
});

test.serial('fallback', async function(t) {
  const jsonRes = await getAppResponse('fallback');
  expect(jsonRes.payload).to.have.deep.keys('google');
  expect(jsonRes.payload.google.expectUserResponse).to.be.true;
  expect(jsonRes.payload.google.richResponse.items[0]).to.have.deep.keys('simpleResponse');
  t.pass();
});

test.serial('resumepodcast', async function(t) {
  const jsonRes = await getAppResponse('resumepodcast');
  expect(jsonRes.payload).to.have.deep.keys('google');
  expect(jsonRes.payload.google.expectUserResponse).to.be.true;
  expect(jsonRes.payload.google.richResponse.items).to.have.lengthOf(2);
  expect(jsonRes.payload.google.richResponse.items[1].mediaResponse).to.include({"mediaType":"AUDIO"});
  t.pass();
});

test.serial('welcome Test', async function(t) {
  const jsonRes = await getAppResponse('welcome');
  console.log("welcome Test ::: ", JSON.stringify(jsonRes));
  expect(jsonRes.payload).to.have.deep.keys('google');
  expect(jsonRes.payload.google.expectUserResponse).to.be.true;
  expect(jsonRes.payload.google.systemIntent.intent).to.equal("actions.intent.OPTION");
  expect(jsonRes.payload.google.systemIntent.data.carouselSelect.items).be.an('array');
  expect(jsonRes.payload.google.richResponse.items).to.have.lengthOf(1);
  expect(jsonRes.payload.google.richResponse.items[0].simpleResponse.textToSpeech).to.equal("<speak>Here are some popular podcasts.</speak>");
  t.pass();
});
