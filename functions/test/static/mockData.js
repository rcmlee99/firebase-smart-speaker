 const episode = {
    "id": "4a4d9663-86b8-496b-9781-ab60016bc468",
    "title": "ASTROPHYSICS BY ASTRO BACHIE",
    "slug": "astrophysics-by-astro-bachie",
    "description": "AKA Dr. Matt Agnew. AKA Rosie’s husband. In our first ever special guest episode, Astro Bachie gives Rosie and Jacob just the gist on Space Guessing! There’s talk of alien predators, the sun blowing up and juuuust a little Bachelor goss.\n\n\nMatt’s Agnew’s Adelaide Fringe show A Guide To Life Beyond Earth runs from Feb 14-23. Get tickets by pasting this link into your browser: http://bit.ly/2Sqe3kU.\n\n\nRosie Waterland’s show Kid Chameleon starts touring nationally on Feb 25. Get tickets by pasting this link into your browser: http://bit.ly/2SH695I.",
    "smallImageUrl": "https://www.omnycontent.com/d/clips/820f09cf-2ace-4180-a92d-aa4c0008f5fb/0aaaaf07-2864-40b6-b489-aa9300627d91/4a4d9663-86b8-496b-9781-ab60016bc468/image.jpg?size=small",
    "audioUrl": "https://traffic.omny.fm/d/clips/820f09cf-2ace-4180-a92d-aa4c0008f5fb/0aaaaf07-2864-40b6-b489-aa9300627d91/4a4d9663-86b8-496b-9781-ab60016bc468/audio.mp3",
    "durationSeconds": 2889.43,
    "podcast": {
        "id": "6cb06e66-54a9-46ac-9815-aa930063b04c",
        "name": "Just the Gist",
        "slug": "just-the-gist",
        "heroImageMobileUrl": "https://podcastone-img-dev.scalabs.com.au/api/assets/a8d2e9e7-9e26-4dad-b531-11e55876bc90?version=0"
    }
};

const welcomeConv = {
    "body": {
        "queryResult" : { "responseId": "c76595eb-0919-486b-b168-476586471b07-7f245a81", "queryResult": { "queryText": "GOOGLE_ASSISTANT_WELCOME", "action": "input.welcome", "parameters": {}, "allRequiredParamsPresent": true, "outputContexts": [ { "name": "projects/test-podcastone/agent/sessions/ABwppHHRabn3-ZbUkhMI0YGuYHV9ShuCXCt1ektkkNZ0xK2WDDphaqJPuUZQ7cd5bd0AoKOKKDUll5Vuf3vw/contexts/defaultwelcomeintent-followup", "lifespanCount": 2 } ], "intent": { "name": "projects/test-podcastone/agent/intents/25bcfe4d-94d4-4a4d-bd59-acb7ed9b230f", "displayName": "Default Welcome Intent" }, "intentDetectionConfidence": 1, "languageCode": "en" }, "originalDetectIntentRequest": { "source": "google", "version": "2", "payload": { "user": { "locale": "en-US", "lastSeen": "2020-02-19T03:06:37Z", "userVerificationStatus": "VERIFIED" }, "conversation": { "conversationId": "ABwppHHRabn3-ZbUkhMI0YGuYHV9ShuCXCt1ektkkNZ0xK2WDDphaqJPuUZQ7cd5bd0AoKOKKDUll5Vuf3vw", "type": "NEW" }, "inputs": [ { "intent": "actions.intent.MAIN", "rawInputs": [ { "inputType": "VOICE", "query": "Talk to Podcast One Alpha" } ] } ], "surface": { "capabilities": [ { "name": "actions.capability.ACCOUNT_LINKING" }, { "name": "actions.capability.MEDIA_RESPONSE_AUDIO" }, { "name": "actions.capability.SCREEN_OUTPUT" }, { "name": "actions.capability.AUDIO_OUTPUT" } ] }, "isInSandbox": true, "requestType": "SIMULATOR" } }, "session": "projects/test-podcastone/agent/sessions/ABwppHHRabn3-ZbUkhMI0YGuYHV9ShuCXCt1ektkkNZ0xK2WDDphaqJPuUZQ7cd5bd0AoKOKKDUll5Vuf3vw" }
    }
};

 const mockFirestoreData = {
     "user" : {
         "uid": "hiMWz6IM6mWi8LECvqPajEUEL9x1"
     },
     "action" : "addOrUpdateUserFavouritePodcast",
     "episode" : {
         "id" : "4a4d9663-86b8-496b-9781-ab60016bc468",
         "durationSeconds": 2889.43
     },
     "podcastId" : "5dec054c-97ed-4104-92a9-aa4c003f9f05"
 };
 const testUserId = "111222333";
 const changePassword= 'passwordreset';
 const changeEmail= 'unit-testing-firebase-change-email@testing.com';
 const createUser = {
     newuser: {
         uid: `${testUserId}`,
             email: 'unit-testing-firebase@testing.com',
             emailVerified: false,
             password: 'testpassword',
     }
 };


module.exports = {
    episode,
    welcomeConv,
    mockFirestoreData,
    createUser,
    testUserId,
    changePassword,
    changeEmail
};
