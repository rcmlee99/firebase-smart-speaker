# Test JSON files
This directory contains JSON files, each containing a request coming from Dialogflow to your fulfillment code. Please see [official docs](https://developers.google.com/assistant/actions/build/json) for full reference of the request format. Those files are sent to your fulfillment as part of unit tests in `functions/test/index-test.js`.
