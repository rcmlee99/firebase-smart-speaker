const test = require('ava');
const admin = require("firebase-admin");
const request = require('supertest');
const {userServiceManagerTest} = require('../userServiceManager');
const {squidex: {secretkey}} = require('../config/local.json');
const { createUser, testUserId, changeEmail, changePassword } = require('./static/mockData');


test.serial('Correct response from health route', async t => {
  const result = await request(userServiceManagerTest).get('/');
  t.is(result.status, 200);
  t.is(result.text, 'Service OK');
});

test.serial('Throws error when authorisation headers are missing', async t => {
  const result = await request(userServiceManagerTest).post('/');
  t.is(result.status, 401);
  t.is(result.body.message, 'Missing Authorization Header');
});

test.serial('Throws error when authorisation token is incorrect', async t => {
  const result = await request(userServiceManagerTest)
      .post('/')
      .set('Authorization', `Basic wrongkey`);
  t.is(result.status, 401);
  t.is(result.body.message, 'Authentication required.');
});

test.serial('Successfully create a user', async t => {
  const result = await request(userServiceManagerTest)
      .post('/')
      .set('Authorization', `Basic ${secretkey}`)
      .set('action', 'create')
      .send(createUser);
  t.is(result.status, 200);
  t.is(result.body.uid, createUser.newuser.uid);
  t.is(result.body.email, createUser.newuser.email);
  t.is(result.body.emailVerified, createUser.newuser.emailVerified);
});

test.serial('Successfully edit a user', async t => {
  const result = await request(userServiceManagerTest)
      .post('/')
      .set('Authorization', `Basic ${secretkey}`)
      .set('action', 'edit')
      .set('uid', `${testUserId}`)
      .send({
        payload: {
          email: `${changeEmail}`,
          emailVerified: true,
          password: `${changePassword}`,
        }
      });
  t.is(result.status, 200);
  t.is(result.body.email, changeEmail);
  t.is(result.body.emailVerified, true);
});

test.serial('Successfully delete a user', async t => {
  const result = await request(userServiceManagerTest)
      .post('/')
      .set('Authorization', `Basic ${secretkey}`)
      .set('action', 'delete')
      .set('uid', `${testUserId}`);
  t.is(result.status, 200);
});
