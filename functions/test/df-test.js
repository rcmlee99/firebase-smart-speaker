// This file contains unit tests for a Dialogflow agent.
'use strict';

const {expect} = require('chai');
const {DialogflowApiFactory} = require('./lib/df-api.js');
const uuid = require('uuid');
const test = require('ava');

/* ====== Substitute the following variables (START) ===== */
const projectId = 'test-podcastone';
// Make sure the service account has "Dialogflow API Client" in GCP IAM
const pathToServiceAccount = './key.json';
// The value itself doesn't matter (can be anything).
const sessionId = uuid.v1();
/* ====== Substitute the following variables (END) ======= */

const serviceAccount = require(pathToServiceAccount);

let dialogflow = undefined;

test.before(async function(t) {
  dialogflow = await DialogflowApiFactory.create({
    projectId: projectId,
    serviceAccount: serviceAccount,
    sessionId: sessionId,
  });
});

test.afterEach(async function(t) {
  await dialogflow.clearSession(sessionId);
});

  /*
  This test checks that Dialogflow correctly matches "Play Hamish Andy" query to a "Play Podcast" intent, and extracts the "podcast"
  entity.
  */
 test.serial('Play Hamish Andy', async function(t) {
  const resJson = await dialogflow.detectIntent('Play Hamish Andy');
  //console.log('Dialogflow Test Result Play Hamish Andy :::', resJson);
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'hamish-andy',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play Agriminders', async function(t) {
  const resJson = await dialogflow.detectIntent('Play Agriminders');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'agriminders',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play Agri minders (2)', async function(t) {
  const resJson = await dialogflow.detectIntent('Play Agri minders');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'agriminders',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play Allergies', async function(t) {
  const resJson = await dialogflow.detectIntent('Play Allergies');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'allergies',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play Cat\'s Pyjamas with The Chaser', async function(t) {
  const resJson = await dialogflow.detectIntent('Play Cat\'s Pyjamas with The Chaser');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'cats-pyjamas-with-the-chaser',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play Change Makers', async function(t) {
  const resJson = await dialogflow.detectIntent('Play Change Makers');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'change-makers',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play Dognative Therapy', async function(t) {
  const resJson = await dialogflow.detectIntent('Play Dognative Therapy');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'dognitive-therapy',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play Dog native Therapy (2)', async function(t) {
  const resJson = await dialogflow.detectIntent('Play Dog Native Therapy');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'dognitive-therapy',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play From The Inside with Peter Rix', async function(t) {
  const resJson = await dialogflow.detectIntent('Play From The Inside with Peter Rix');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'from-the-inside',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play Just The Gist', async function(t) {
  const resJson = await dialogflow.detectIntent('Play Just The Gist');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'just-the-gist',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play Kochie Easy Steps to Financial Success', async function(t) {
  const resJson = await dialogflow.detectIntent('Play Kochie Easy Steps to Financial Success');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'kochie-s-easy-steps-to-financial-success',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play Kochie Guide to Starting Your Own Business', async function(t) {
  const resJson = await dialogflow.detectIntent('Play Kochie Guide to Starting Your Own Business');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'kochie-s-guide-to-starting-your-own-business',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play Media Week', async function(t) {
  const resJson = await dialogflow.detectIntent('Play Media Week');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'mediaweek-tv',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play Prodcast Powerful Radio Production', async function(t) {
  const resJson = await dialogflow.detectIntent('Play Prodcast Powerful Radio Production');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'prodcast',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play Scamming The Scammer', async function(t) {
  const resJson = await dialogflow.detectIntent('Play Scamming The Scammer');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'scamming-the-scammer',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play Superwomen we aint', async function(t) {
  const resJson = await dialogflow.detectIntent('Play Superwomen we aint');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'superwomen-we-aint',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play The Big Podcast with The Shaq', async function(t) {
  const resJson = await dialogflow.detectIntent('Play The Big Podcast with The Shaq');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'the-big-podcast-with-shaq',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play The Game Plan with Dr Nick Krasner', async function(t) {
  const resJson = await dialogflow.detectIntent('Play The Game Plan with Dr Nick Krasner');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'the-game-plan',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play The Moment', async function(t) {
  const resJson = await dialogflow.detectIntent('Play The Moment');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'the-moment-with-mark-howard',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play The Wrap', async function(t) {
  const resJson = await dialogflow.detectIntent('Play The Wrap');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'the-wrap',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play Fast Track Career Conversations with Margie Hartley', async function(t) {
  const resJson = await dialogflow.detectIntent('Play Fast Track Career Conversations with Margie Hartley');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'fasttrack-career-conversations-with-margie-hartley',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play Pollen', async function(t) {
  const resJson = await dialogflow.detectIntent('Play Pollen');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'pollen',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play The Energy Truth', async function(t) {
  const resJson = await dialogflow.detectIntent('Play The Energy Truth');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'the-energy-truth',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play Puka Up with Wayne Schwass', async function(t) {
  const resJson = await dialogflow.detectIntent('Play Puka Up with Wayne Schwass');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'puka-up-with-wayne-schwass',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play Greta Lee Jackson', async function(t) {
  const resJson = await dialogflow.detectIntent('Play Greta Lee Jackson');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'fail-with-greta-lee-jackson',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play Better For It', async function(t) {
  const resJson = await dialogflow.detectIntent('Play Better For It');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'better-for-it',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play Mark Pesce Betabank', async function(t) {
  const resJson = await dialogflow.detectIntent('Play Mark Pesce Betabank');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'mark-pesce-betabank',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play The Aussie Spirit Podcast', async function(t) {
  const resJson = await dialogflow.detectIntent('Play The Aussie Spirit Podcast');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'the-aussie-spirit',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('Play Understate Lucille Butterworth', async function(t) {
  const resJson = await dialogflow.detectIntent('Play Understate Lucille Butterworth');
  expect(resJson.queryResult).to.include.deep.keys('parameters');
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    'podcast': 'under-state-one-lucille',
  });
  expect(resJson.queryResult.intent.displayName).to.equal('Play podcast');
  t.pass();
});

test.serial('I don\'t know', async function(t) {
  const resJson = await dialogflow.detectIntent('I don\'t know');
  expect(resJson.queryResult.fulfillmentText).to.equal('Here are a few things you can ask me to do. Ask me to play a podcast by saying its name or say \'popular\' to hear the most popular podcasts.');
  expect(resJson.queryResult.intent.displayName).to.equal('Help');
  t.pass();
});

test.serial('Popular', async function(t) {
  const resJson = await dialogflow.detectIntent('Popular');
  expect(resJson.queryResult.fulfillmentText).to.equal('<speak>Just say the name of one of these popular podcasts to start listening<p><s>Birth, Baby &amp; Beyond</s><break time="0.5s"/><s>Agriminders</s><break time="0.5s"/><s>The Wellness Collective</s><break time="0.5s"/>.</p> What would you like to hear?</speak>');
  expect(resJson.queryResult.intent.displayName).to.equal('Recommend');
  t.pass();
});

test.serial('Recommend', async function(t) {
  const resJson = await dialogflow.detectIntent('Recommend');
  expect(resJson.queryResult.fulfillmentText).to.equal('<speak>Just say the name of one of these popular podcasts to start listening<p><s>Birth, Baby &amp; Beyond</s><break time="0.5s"/><s>Agriminders</s><break time="0.5s"/><s>The Wellness Collective</s><break time="0.5s"/>.</p> What would you like to hear?</speak>');
  expect(resJson.queryResult.intent.displayName).to.equal('Recommend');
  t.pass();
});

test.serial('Resume podcast', async function(t) {
  const resJson = await dialogflow.detectIntent('Resume podcast');
  expect(resJson.queryResult.fulfillmentText).to.equal('Cannot display response in Dialogflow simulator. Please test on the Google Assistant simulator instead.');
  expect(resJson.queryResult.intent.displayName).to.equal('Resume podcast');
  expect(resJson.webhookStatus.message).to.equal('Webhook execution successful');
  t.pass();
});

test.serial('Next podcast', async function(t) {
  const resJson = await dialogflow.detectIntent('Next podcast');
  expect(resJson.webhookStatus.message).to.equal('Webhook execution successful');
  expect(resJson.queryResult.intent.displayName).to.equal('Next podcast');
  t.pass();
});

test.serial('Previous podcast', async function(t) {
  const resJson = await dialogflow.detectIntent('Previous podcast');
  expect(resJson.webhookStatus.message).to.equal('Webhook execution successful');
  expect(resJson.queryResult.intent.displayName).to.equal('Previous podcast');
  t.pass();
});
