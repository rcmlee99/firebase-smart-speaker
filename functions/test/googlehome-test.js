// This file contains unit tests for a Google Home Object.

const test = require('ava');
const { expect } = require('chai');
const { 
    episodeMediaObject,
    podcastCarouselObject,
    podcastOneObject,
    exitCardObject,
    signInCardObject,
    splashCardObject,
    sendConvObject,
    podcastListObject,
} = require('../model/googleHomeObject');
const constants = require("./../constants");
const audioLink = constants.audioLink;
const TAGLINE = constants.TAGLINE;
const cardImageUrl = constants.cardImageUrl;
const SKILL_NAME = constants.SKILL_NAME;
const { episode } = require("./static/mockData");
var recommendedList = require('./../data/recommendedList.json');

test.serial('googleHomeObject exitCardObject', async (t) => {
    const result = await exitCardObject();
    expect(result).to.be.an('object');
    expect(result.formattedText).to.equal('Thanks for using Podcast One Australia');
    expect(result.image.url).to.equal(cardImageUrl);
    expect(result.image.accessibilityText).to.equal(SKILL_NAME);
    t.pass();
});

test.serial('googleHomeObject signInCardObject', async (t) => {
    const result = await signInCardObject();
    expect(result).to.be.an('object');
    expect(result.intent).to.equal('actions.intent.SIGN_IN');
    expect(result.inputValueData.optContext).to.equal("You need to link your Podcast One Australia Account with Google Assistant App. Follow the link in the Google Assistant App to continue");
    t.pass();
});

test.serial('googleHomeObject splashCardObject', async (t) => {
    const result = await splashCardObject();
    expect(result).to.be.an('object');
    expect(result.formattedText).to.equal(' ');
    expect(result.imageDisplayOptions).to.equal("CROPPED");
    t.pass();
});

test.serial('googleHomeObject episodeMediaObject', async (t) => {
    const result = await episodeMediaObject(episode);
    console.log(`result :::`, JSON.stringify(result));
    expect(result).to.be.an('object');
    expect(result.name).to.be.an('string');
    expect(result.contentUrl).to.be.an('string');
    expect(result.description).to.be.an('string');
    expect(result.largeImage.url).to.be.an('string');
    expect(result.largeImage.accessibilityText).to.be.an('string');
    t.pass();
});

test.serial('googleHomeObject podcastOneObject', async (t) => {
    const result = await podcastOneObject(episode);
    expect(result).to.be.an('object');
    expect(result.title).to.be.an('string');
    expect(result.subtitle).to.be.an('string');
    expect(result.formattedText).to.be.an('string');
    expect(result.image.url).to.be.an('string');
    expect(result.image.accessibilityText).to.be.an('string');
    expect(result.imageDisplayOptions).to.be.an('string');
    t.pass();
});

test.serial('googleHomeObject podcastCarouselObject', async (t) => {
    const result = await podcastCarouselObject(recommendedList.popularPodcasts);
    expect(result).to.be.an('object');
    expect(result.intent).to.equal('actions.intent.OPTION');
    expect(result.inputValueData.carouselSelect.items).to.be.an('array');
    t.pass();
});

test.serial('googleHomeObject podcastListObject', async (t) => {
    const result = await podcastListObject(recommendedList.popularPodcasts);
    expect(result).to.be.an('object');
    expect(result.intent).to.equal('actions.intent.OPTION');
    expect(result.inputValueData.listSelect.items).to.be.an('array');
    t.pass();
});

