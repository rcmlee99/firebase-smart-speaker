const { getRecommendedList, getPodcastEpisodes } = require('./service/graphqlService');
const functions = require('firebase-functions');

const apisynthetic = functions.pubsub.schedule('every 20 minutes').onRun(async (context) => {
    console.log('Dev GraphQL check every 20 minutes');
    try {
      const recommendedList = await getRecommendedList();
      const podcastQuery = await getPodcastEpisodes("hamish-andy");
      const result = {
        recommendedList : recommendedList,
        podcastQuery : podcastQuery
      }
      console.log("result :::", JSON.stringify(result));
      return JSON.stringify(result);
    } catch (err) {
      console.error(err)
      return err;
    }    
  });

module.exports = {
    apisynthetic
};