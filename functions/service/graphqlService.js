const functions = require('firebase-functions');
const { GraphQLClient } = require('graphql-request');
const config = require(`../config/test.json`);
const pageSize = 5;
const GRAPHQL_ENDPOINT = (process.env.NODE_ENV === 'local') ? config.graphql.endpoint : functions.config().graphql.endpoint;

// Initialize GraphQL
const clientql = new GraphQLClient(GRAPHQL_ENDPOINT);
console.log("GRAPHQL_ENDPOINT :::", GRAPHQL_ENDPOINT);

const getPodcastEpisodes = (slugName) => {
    const query = `{
        podcast(where:{slug:"${slugName}"}){
            id
            name
            numberOfEpisodes
            episodes(page:{size:${pageSize}, number: 1}){
            totalPages     
            items{
                id
            }
        }
        }
    }`;
    return new Promise((resolve, reject) => {
        clientql.request(query).then(json => {
            resolve(json);
        }).catch(error => {
            reject(error);
        });
    });
}

const getPodcastEpisodesById = (podcastId) => {
    const query = `{
        podcast(where:{id:"${podcastId}"}){
            id 
            slug 
            name
            description
            heroImageMobileUrl
            numberOfEpisodes
            episodes(page:{size:${pageSize}, number: 1}){
            totalPages     
            items{
                id
            }
        }
        }
    }`;
    return new Promise((resolve, reject) => {
        clientql.request(query).then(json => {
            var newjson = json;
            for (var key in newjson.podcast) {
                if (key === "description") {
                    newjson.podcast.description = removeHyperLink(json.podcast.description);
                }
            }
            console.log(`getPodcastEpisodesById :::`, JSON.stringify(newjson))
            return newjson;
        }).then(json => {
            resolve(json);
        }).catch(error => {
            reject(error);
        });
    });
}

const removeHyperLink = (text) => {
    const textArray = text.trim().split(". ");
    const newTextArray = [];
    // console.log(`old description :::`, text);
    // console.log(`***************`);
    // console.log(`textArray :::`, JSON.stringify(textArray));
    textArray.forEach(item => {
        /* eslint-disable no-useless-escape */
        if (item.match(/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g)) {
            // remove url pattern
        } else if (item.match(/^(.*?(\www\b)[^$]*)$/g)) {
            // remove www without the http or https
        } else {
            // console.log(`don't match`)
            newTextArray.push(item);
        }
        /* eslint-enable no-useless-escape */
    });
    // console.log(`newTextArray :::`, JSON.stringify(newTextArray));

    // Check and add back fullstop at the end of sentence
    var newDescription = newTextArray.join(". ");
    if (newDescription.substr(-1) !== ".") {
        newDescription = newDescription + ".";
    }
    // console.log(`new description :::`, newDescription);
    // console.log(`*****************`)
    return newDescription;
}

const getRecommendedList = () => {
    const query = `{
        popularPodcasts {
            id 
            slug 
            name
            description
            heroImageMobileUrl
        }
    }`;
    return new Promise((resolve, reject) => {
        clientql.request(query).then(json => {
            json.popularPodcasts.map(item => removeHyperLink(item.description))
            return json;
        }).then(json => {
            resolve(json);
        }).catch(error => {
            reject(error);
        });
    });
}

const getEpisodeDetail = (episodeId) => {
    // console.log("GraphQLService episodeId::: ", episodeId);
    const query = `{
        episode (where: {id: "${episodeId}"}) {
            id
            title
            slug
            description
            smallImageUrl
            audioUrl
            durationSeconds
            podcast {
                id
                name
                slug
                heroImageMobileUrl
            }
        }
    }`;
    return new Promise((resolve, reject) => {
        clientql.request(query).then(json => {
            var newjson = json;
            for (var key in newjson.episode) {
                if (key === "description") {
                    newjson.episode.description = removeHyperLink(json.episode.description);
                }
            }
            console.log(`getEpisodeDetail :::`, JSON.stringify(newjson))
            return newjson;
        }).then(json => {
            resolve(json);
        }).catch(error => {
            reject(error);
        });
    });
}


module.exports = {
    getPodcastEpisodes,
    getPodcastEpisodesById,
    getEpisodeDetail,
    getRecommendedList
};
