const dialogflow = require('dialogflow');
const functions = require('firebase-functions');

const credentials = require('../test/key.json');
var projectID = 'test-podcastone';

if (process.env.NODE_ENV === 'production') {
    projectID = functions.config().dialogflow.clientid;
}

var entitiesClient = new dialogflow.EntityTypesClient();
if (process.env.NODE_ENV === 'local') {
    entitiesClient = new dialogflow.EntityTypesClient({
        credentials: credentials,
    });
}
var agentPath = entitiesClient.projectAgentPath(projectID);

/** Define a custom error object to help control flow in our Promise chain. */
class EntityNotFoundError extends Error {}

// Tell client library to call Dialogflow with a request to
// list all EntityTypes.
const findEntities = (entityName) => {
    return new Promise((resolve, reject) => {
        entitiesClient.listEntityTypes({parent: agentPath})
        // Go through all EntityTypes and find the one we wish to update
        // (in this case, the EntityType named 'Podcast').
        .then((responses) => {
            // The array of EntityTypes is the 0th element of the response.
            const resources = responses[0];
            //console.log('resources', JSON.stringify(resources));
            // Loop through and find the EntityType we wish to update.
            for (let i = 0; i < resources.length; i++) {
                const entity = resources[i];
                if (entity.displayName === entityName) {
                    //console.log('entityResult', JSON.stringify(entity));
                    resolve(entity);
                }
            }
            // If we couldn't find the expected entity, throw a custom error.
           reject('Entity not found');
        }).catch((err) => {
            console.error('Error :', err);
            reject('Error :', err);
        });
    })
}

const updateEntities = (resultFindEntities, entitiesData, entityValue) => {
    return new Promise((resolve, reject) => {
        resultFindEntities.entities = entitiesData
        const request = {
            entityType: resultFindEntities,
            // Tell the API to only modify the 'entities' field, not any other
            // fields of the EntityType.
            updateMask: {
            paths: ['entities'],
            },
        };
        // Tell Dialogflow to update the EntityType.
        entitiesClient.updateEntityType(request).then((responses) => {
            console.log(`Success: Entity slug ${entityValue} updated in Dialogflow`);
            resolve(responses);
        }).catch((err) => {
            console.error('Error :', err);
            reject('Error :', err);
        });
    })
}

const deleteEntities = (entityTypePath, entityValue) => {
    return new Promise((resolve, reject) => {

        const request = {
            parent: entityTypePath,
            entityValues: [entityValue],
          };

        // Tell Dialogflow to delete the EntityType.
        entitiesClient.batchDeleteEntities(request).then((responses) => {
            console.log(`Success: Entity slug ${entityValue} removed in Dialogflow`);
            resolve(responses);
        }).catch((err) => {
            console.error('Error :', err);
            reject('Error :', err);
        });
    })
}

module.exports = {
    findEntities,
    updateEntities,
    deleteEntities
};