const firebase = require("firebase");
require("firebase/auth");
require("firebase/firestore");
require("firebase/storage");
const functions = require('firebase-functions');
const {getISOStringWithoutMillisec} = require('../utilities/formatDate');

if (process.env.NODE_ENV === 'production') {
    // Initialize Cloud Firestore through Firebase
    var configFirebase = {
        apiKey: functions.config().fb.apikey,
        authDomain: functions.config().fb.authdomain,
        projectId: functions.config().fb.projectid,
    };
    firebase.initializeApp(configFirebase);
}

const getCurrentUser = (accessToken) => {
    return new Promise((resolve, reject) => {
        firebase
            .auth()
            .signInWithCustomToken(accessToken)
            .then(() => {
                resolve(firebase.auth().currentUser);
            })
            .catch((error) => {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                console.log(error.message);
                reject(error);
            });
    });
};

const addOrUpdateUserEpisode = async (conv, episode) => {
  if (conv.data.firebaseuser) {
    const dateISOString = getISOStringWithoutMillisec(new Date());
    const payload = {
        episodeId: episode.id,
        duration: episode.durationSeconds,
        isMarkedAsPlayed: false,
        playHeadPosition: 0,
        playedDateTime: dateISOString,
      };
    firebase.firestore()
        .collection('users')
        .doc(`${conv.data.firebaseuser.uid}`)
        .collection('episodes')
        .doc(episode.id)
        .set(payload);
  }
};

const addOrUpdateUserFavouritePodcast = async (conv, podcastId) => {
  if (conv.data.firebaseuser) {
    const dateISOString = getISOStringWithoutMillisec(new Date());
    const payload = {
        episodeSortOrder: "latest",
        podcastId: podcastId,
        lastSeen: dateISOString,
      };
    await firebase.firestore()
        .collection('users')
        .doc(`${conv.data.firebaseuser.uid}`)
        .collection('favouritePodcasts')
        .doc(podcastId)
        .set(payload);
  }
};

const getUserLastEpisode = async (user) => {
    var episodes = {};
    // Get the last played episodes of Podcast
    let episodesSnapshot = await firebase.firestore()
        .collection('users')
        .doc(`${user.uid}`)
        .collection('episodes')
        .orderBy('playedDateTime', 'desc')
        .limit(1)
        .get();
    if (episodesSnapshot.size>0) {
        episodesSnapshot.forEach((doc) => {
            const { episodeId, playHeadPosition, playedDateTime, duration, isMarkedAsPlayed } = doc.data();
            episodes[0] = {
                episodeId,
                playHeadPosition,
                playedDateTime,
                duration,
                isMarkedAsPlayed,
            };
        });
        return episodes[0];
    } else {
        return null;
    }
};

const getUserFavShows = async (user) => {
    const favShow = [];
    // Get the last favourite show of Podcast
    let favSnapshot = await firebase.firestore()
    .collection('users')
    .doc(`${user.uid}`)
    .collection('favouritePodcasts')
    .orderBy('lastSeen', 'desc')
    .get();
    if (favSnapshot.size>0) {
        favSnapshot.forEach((doc, index) => {
            const { lastSeen, podcastId } = doc.data();
            favShow.push(podcastId)
        });
        return favShow;
    } else {
        return null;
    }
};

module.exports = {
    getCurrentUser,
    getUserLastEpisode,
    getUserFavShows,
    addOrUpdateUserEpisode,
    addOrUpdateUserFavouritePodcast
};
