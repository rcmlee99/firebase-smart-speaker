const functions = require('firebase-functions');
const sgMail = require('@sendgrid/mail');

const SENDGRID_API_KEY = functions.config().sendgrid ? functions.config().sendgrid.key : 'key';
const PROJECT = process.env.GCLOUD_PROJECT;

sgMail.setApiKey(SENDGRID_API_KEY);

exports.sendMail = async msg => {
  try {
    await sgMail.send(msg);
    console.log(`email sent! => ${PROJECT} => to:${msg.to}, from:${msg.from}, templateId:${msg.templateId}`);
    return 'email sent!';
  } catch (e) {
    console.log('error', e);
    return e;
  }
};