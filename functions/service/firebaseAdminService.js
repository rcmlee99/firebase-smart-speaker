const fs = require('fs');
const admin = require("firebase-admin");
// see https://www.npmjs.com/package/uuid
const uuidv4 = require("uuid/v4");
const {getISOStringWithoutMillisec} = require('../utilities/formatDate');

const env = process.env.NODE_ENV || 'local';
const config = require(`../config/${env}.json`);
const credentials = require('../test/key.json');

let firestore;
let auth;
let bucket;

const initialiseFirebaseAdmin = async () => {
    if (admin.apps.length === 0) {
        if (process.env.NODE_ENV === 'local') {
            admin.initializeApp({
                credential: admin.credential.cert(credentials),
                storageBucket: config.bucket
            });
        } else {
            admin.initializeApp();
        }
    }

    firestore = admin.firestore();
    auth = admin.auth();
    bucket = admin.storage().bucket();
};

const getUserFavShows = async (jsonData) => {
    const favShow = [];
    // Get the last favourite show of Podcast
    let favSnapshot = await firestore
        .collection('users')
        .doc(`${jsonData.user.uid}`)
        .collection('favouritePodcasts')
        .orderBy('lastSeen', 'desc')
        .limit(3)
        .get();
    if (favSnapshot.size>0) {
        favSnapshot.forEach((doc) => {
            const { podcastId } = doc.data();
            console.log("doc :::", doc.data());
            favShow.push(podcastId)
        });
        return favShow;
    } else {
        return null;
    }
};

// Return lastest played episode based on firebase uid
const getUserLastEpisode = async (jsonData) => {
    console.log(`jsonData :::`, JSON.stringify(jsonData));
    var episodes = {};
    // Get the last played episodes of Podcast
    let episodesSnapshot = await firestore
        .collection('users')
        .doc(`${jsonData.user.uid}`)
        .collection('episodes')
        .orderBy('playedDateTime', 'desc')
        .limit(1)
        .get();
    if (episodesSnapshot.size>0) {
        episodesSnapshot.forEach((doc) => {
            const { episodeId, playHeadPosition, playedDateTime, duration, isMarkedAsPlayed } = doc.data();
            console.log("doc :::", doc.data());
            episodes[0] = {
                episodeId,
                playHeadPosition,
                playedDateTime,
                duration,
                isMarkedAsPlayed,
            };
        });
        return episodes[0];
    } else {
        return null;
    }
};

const addOrUpdateUserEpisode = async (jsonData) => {
    const dateISOString = getISOStringWithoutMillisec(new Date());
    const payload = {
        episodeId: jsonData.episode.id,
        duration: jsonData.episode.durationSeconds,
        isMarkedAsPlayed: jsonData.episode.isMarkedAsPlayed || false,
        playHeadPosition: jsonData.episode.playHeadPosition || 0,
        playedDateTime: dateISOString,
    };
    return firestore
        .collection('users')
        .doc(`${jsonData.user.uid}`)
        .collection('episodes')
        .doc(jsonData.episode.id)
        .set(payload);
};

const addOrUpdateUserFavouritePodcast = async (jsonData) => {
    const dateISOString = getISOStringWithoutMillisec(new Date());
    const payload = {
        episodeSortOrder: "latest",
        podcastId: jsonData.podcastId,
        lastSeen: dateISOString,
    };
    return firestore
        .collection('users')
        .doc(`${jsonData.user.uid}`)
        .collection('favouritePodcasts')
        .doc(jsonData.podcastId)
        .set(payload);
};

async function generateAuthCode(jsonData) {
    // Generate auth code which we will later use to get our auth_token.
    let authCode = uuidv4();

    // Update the authCode in user collection
    await firestore.doc(`users/${jsonData.uid}`).update({
      authCode: authCode,
      uid: jsonData.uid,
      lastUpdateDate: getISOStringWithoutMillisec(new Date())
    });
    const result = {
        state : jsonData.state,
        code : authCode
    }
    return result;
}

async function getAccessTokenByRefreshToken(req, res) {
    let refresh_token = req.body.refresh_token;
    let client_idSecret = Buffer.from(req.headers.authorization.split(" ")[1], 'base64').toString();
    let client_id = client_idSecret.split(":")[0];
    let refreshKey;

    if (client_id==="alexa") {
        refreshKey = "refreshTokenAlexa";
    } else if (client_id==="ghome") {
        refreshKey = "refreshTokenGhome";
    }

    let dbEntry = await firestore
        .collection("users")
        .where(refreshKey, "==", refresh_token)
        .get();

    if (dbEntry.empty) {
        return res.send(404);
    }

    let dbDoc = dbEntry.docs[0];

    let uid = dbDoc.data().uid;

    const additionalClaims = {};
    if (client_id==="alexa") {
        additionalClaims["alexa"] = true
    } else if (client_id==="ghome") {
        additionalClaims["ghome"] = true
    }

    let access_token = await admin
        .auth()
        .createCustomToken(uid, additionalClaims);

    const addTokens = {};
    if (client_id==="alexa") {
        addTokens["accessTokenAlexa"] = access_token
    } else if (client_id==="ghome") {
        addTokens["accessTokenGhome"] = access_token
    }

    firestore.doc(`users/${uid}`).update(addTokens);

    return res.json({
        access_token: access_token,
        token_type: "Bearer",
        refresh_token: refresh_token,
        expires_in: 60 * 60,
        id_token: ""
    });
}

async function getAccessTokenByAuthCode(req, res) {
    let authCode = req.body.code;
    let client_idSecret = Buffer.from(req.headers.authorization.split(" ")[1], 'base64').toString();
    let client_id = client_idSecret.split(":")[0];

    let dbEntry = await firestore
        .collection("users")
        .where("authCode", "==", authCode)
        .get();

    if (dbEntry.empty) {
        return res.send(404);
    }

    let dbDoc = dbEntry.docs[0];

    let uid = dbDoc.data().uid;

    const additionalClaims = {};
    if (client_id==="alexa") {
        additionalClaims["alexa"] = true
    } else if (client_id==="ghome") {
        additionalClaims["ghome"] = true
    }

    // Create a custom token. See https://firebase.google.com/docs/auth/admin/create-custom-tokens
    let access_token = await admin
        .auth()
        .createCustomToken(uid, additionalClaims);

    let refresh_token = uuidv4();

    const addTokens = {};
    if (client_id==="alexa") {
        addTokens["accessTokenAlexa"] = access_token;
        addTokens["refreshTokenAlexa"] = refresh_token
    } else if (client_id==="ghome") {
        addTokens["accessTokenGhome"] = access_token;
        addTokens["refreshTokenGhome"] = refresh_token
    }

    firestore.doc(`users/${uid}`).update(addTokens);

    return res.json({
        access_token: access_token,
        token_type: "Bearer",
        refresh_token: refresh_token,
        expires_in: 60 * 60,
        id_token: ""
    });
}

const updateBucket = (filename, responses, jsonData) => {
    return new Promise((resolve, reject) => {
        // Store the JSON object in tmp filename
        fs.writeFile('/tmp/'+filename, JSON.stringify(responses), function(err) {
                if (err) throw err;
                console.log('JSON file created');
            }
        );
        // Update the storage bucket
        bucket.upload('/tmp/'+filename, {
            gzip: false,
            metadata: {
                cacheControl: 'no-cache',
            },
        }).then(() => {
            console.log(`Success: Entity slug ${jsonData.slug} updated in Storage Bucket`);
            resolve(`Success: Entity slug ${jsonData.slug} updated in Storage Bucket`);
        }).catch((err) => {
            console.error('Error updating Storage Bucket:', err);
            reject('Error updating Storage Bucket:', err);
        });
    })
};

const getBucket = (filename) => {
    return new Promise(async (resolve, reject) => {
        const downloadedData = await bucket.file(filename).download().then((downloadedData) => {
            if (downloadedData.toString()==='undefined') {
                console.error('Error file: undefined');
                reject('Error file: undefined');
            }
            let entitiesData = JSON.parse(downloadedData.toString('utf8'));
            resolve(entitiesData);
        }).catch((err) => {
            console.error('Error retrieving Storage Bucket file:', JSON.stringify(err.message));
            reject(`Error retrieving Storage Bucket file: ${JSON.stringify(err.message)}`);
        });
    })
};

const getAllUsers = async (limit = 1000, nextPageToken= undefined, pa) => {
    try {
        let allUsers = [];
        const listUsers = await auth.listUsers(limit, nextPageToken);

        listUsers.users.forEach((userRecord) => {
            let userData = userRecord.toJSON();
            allUsers.push(userData);
        });
        return allUsers;
    } catch (e) {
        console.log('getAllUsers service failed - cannot return users details', e);
        throw (e)
    }
};

const editUser = async (uid = null, payload={}) => {
    try {
        if (uid) {
            const updatedUser = await auth.updateUser(uid, payload);
            return updatedUser.toJSON();
        } else {
            throw 'No uid is given';
        }
    } catch (e) {
        console.log('editUser service failed - did not update user details', e);
        throw (e)
    }
};

const deleteUser = async (uid) => {
    try {
        if (uid) {
            await auth.deleteUser(uid);
        } else {
            throw 'No uid is given';
        }
    } catch (e) {
        console.log('deleteUser service failed - did not delete user', e);
        throw (e)
    }
};

 // Use for testing purposes. Do not use it in production. Creating a user from firebase admin will by-pass tracking.
const createUser = async (payload) => {
    try {
        if (payload.email) {
            const createdUser  = await auth.createUser({
                ...payload
            });
            return createdUser.toJSON();
        } else {
            throw 'Cant not create a new user without email address';
        }

    } catch (e) {
        console.log('createUser service failed - did not create user', e);
        throw (e)
    }
};

module.exports = {
    addOrUpdateUserEpisode,
    addOrUpdateUserFavouritePodcast,
    getUserLastEpisode,
    getUserFavShows,
    getAccessTokenByRefreshToken,
    getAccessTokenByAuthCode,
    generateAuthCode,
    updateBucket,
    getBucket,
    getAllUsers,
    editUser,
    deleteUser,
    createUser,
    admin,
    initialiseFirebaseAdmin,
};

