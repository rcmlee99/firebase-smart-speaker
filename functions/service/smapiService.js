const functions = require('firebase-functions');
const env = process.env.NODE_ENV || 'local';
const config = require(`../config/${env}.json`);
const smapiClient = require('node-alexa-smapi')();

var alexaSecret = config.smapi;
var alexaSkillID = config.alexa.skillid;

if (process.env.NODE_ENV === 'production') {
    alexaSecret = {
        refreshToken: functions.config().smapi.refreshtoken,
        clientId: functions.config().smapi.clientid,
        clientSecret: functions.config().smapi.clientsecret,
    } 
    alexaSkillID = functions.config().alexa.skillid;
}
            
// Get the current interaction Model using SMAPI and update the interaction Model
const updateSlots = (jsonData) => {
    return new Promise((resolve, reject) => {
        smapiClient.tokens.refresh(alexaSecret).then(() => {
            smapiClient.interactionModel.get(alexaSkillID, 'development', 'en-AU').then(result => {
                let slots = result.interactionModel.languageModel.types[0].values;
                const newSlot = {
                    "id": jsonData.slug,
                    "name": {
                        "value": jsonData.slug,
                        "synonyms": [
                        jsonData.name
                        ]
                    }
                }
                slots.push(newSlot);
                let newInteractionModel = result;
                newInteractionModel.interactionModel.languageModel.types[0].values = slots;

                // Update the interaction Model with the latest slot using SMAPI
                smapiClient.interactionModel.update(alexaSkillID, 'development', 'en-AU', newInteractionModel.interactionModel).then(result => {
                    console.log(`Success: Entity slug ${jsonData.slug} updated in Alexa`);
                    resolve(`Success: Entity slug ${jsonData.slug} updated in Alexa`);
                }).catch((err) => {
                    console.error('Error updating slot:', err);
                    reject(`Error updating slot named podcast slug ${jsonData.slug}`);
                })
            });
        })
    })
}

// Get the current interaction Model using SMAPI and update the interaction Model
const deleteSlots = (jsonData) => {
    return new Promise((resolve, reject) => {
        smapiClient.tokens.refresh(alexaSecret).then(() => {
            smapiClient.interactionModel.get(alexaSkillID, 'development', 'en-AU').then(result => {
                let slots = result.interactionModel.languageModel.types[0].values;
                const resultSlots = slots.filter((item) => {
                    return item.id !== jsonData.slug;
                })
                let newInteractionModel = result;
                newInteractionModel.interactionModel.languageModel.types[0].values = slots;

                // Update the interaction Model with the latest slot using SMAPI
                smapiClient.interactionModel.update(alexaSkillID, 'development', 'en-AU', newInteractionModel.interactionModel).then(result => {
                    console.log(`Success: Entity slug ${jsonData.slug} removed in Alexa`);
                    resolve(`Success: Entity slug ${jsonData.slug} removed in Alexa`);
                }).catch((err) => {
                    console.error('Error removing slot:', err);
                    reject(`Error removing slot named podcast slug ${jsonData.slug}`);
                })
            });
        })
    })
}

module.exports = {
    updateSlots,
    deleteSlots
};