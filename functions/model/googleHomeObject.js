const {
    MediaObject,
    Image,
    Carousel,
    List,
    BasicCard,
    SignIn,
    SimpleResponse,
    Suggestions
  } = require('actions-on-google')

const episodeMediaObject = (episodeDetail) => {
    //console.log("Episode Detail :::", JSON.stringify(episodeDetail));
    const mediaObject = new MediaObject({
        name: `${episodeDetail.title.replace(/[^\w\s]/gi, '')}`,
        url: episodeDetail.audioUrl,
        description: episodeDetail.description,
        image: new Image({
          url: episodeDetail.smallImageUrl,
          alt: episodeDetail.description,
        }),
    });
    return mediaObject;
};

const exitCardObject = () => {
  const cardObject = new BasicCard({
    text: `Thanks for using Podcast One Australia`,
    image: new Image({
      url: 'https://ghome-oauth-link.firebaseapp.com/img/PC1_512x512.png',
      alt: 'Podcast One Australia',
    }),
    display: 'CROPPED',
  });
  // console.log("exit card :::", JSON.stringify(cardObject));
  return cardObject;
};

const signInCardObject = () => {
  return new SignIn('You need to link your Podcast One Australia Account with Google Assistant App. Follow the link in the Google Assistant App to continue')
}

const splashCardObject = () => {
  const cardObject = new BasicCard({
    text: ` `,
    display: 'CROPPED',
  });
  // console.log("exit card :::", JSON.stringify(cardObject));
  return cardObject;
};

const podcastListObject = (episodes, podcastListLimit) => {
    const listObject =  new List({
        title: `Here are some popular podcasts`,
        items: getCarouselItems(episodes, podcastListLimit)
      });
    //console.log("carouselObject :::", JSON.stringify(carouselObject));
    return listObject;
}

const podcastCarouselObject = (episodes, podcastListLimit) => {
  const carouselObject =  new Carousel({
      title: `Here are some popular podcasts`,
      items: getCarouselItems(episodes, podcastListLimit)
    });
  //console.log("carouselObject :::", JSON.stringify(carouselObject));
  return carouselObject;
}

function getCarouselItems(episodes, podcastListLimit) {
  const carouselitems = {};
  episodes.forEach((podcast, index) => {
    if (index>=podcastListLimit) return;
    const { slug, name, description, heroImageMobileUrl } = podcast;
    carouselitems[slug] = {
      synonyms: [
        name,
        slug
      ],
      title: name,
      description: description,
      image: new Image({
        url: heroImageMobileUrl,
        alt: name,
      }),
      footer: name
    };
  });
  //console.log("Carousel Items :::", JSON.stringify(carouselitems));
  return carouselitems;
}

const podcastOneObject = (episode) => {
  const cardObject = new BasicCard({
    title: episode.podcast.name,
    subtitle: episode.title,
    text: episode.description,
    image: new Image({
      url: episode.podcast.heroImageMobileUrl,
      alt: episode.podcast.name,
    }),
    display: 'CROPPED',
  });
  return cardObject;
};

const sendConvObject = (conv, resp, mediaresp, suggestion) => {
  conv.ask(new SimpleResponse({ speech: resp }));
  if (conv.surface.capabilities.has('actions.capability.SCREEN_OUTPUT')) {
    if (mediaresp) { conv.close(mediaresp); }    
    if (suggestion) { conv.ask(new Suggestions(suggestion)); }
  }
}

module.exports = {
    episodeMediaObject,
    podcastCarouselObject,
    podcastOneObject,
    exitCardObject,
    signInCardObject,
    splashCardObject,
    sendConvObject,
    podcastListObject
};