const getRemainingEpisodesId = (episodes) => {
    let episodeIdList = [];
    episodes.forEach((episode) => {
        const { id } = episode;
        episodeIdList.push(id);
    })
    return episodeIdList;
};

const saveConvEpisodesData = (conv, previousEpisodes, currentEpisode, remainingEpisodes) => {
    conv.data.previousEpisodes = previousEpisodes;
    conv.data.currentEpisode = currentEpisode;
    conv.data.remainingEpisodes = remainingEpisodes;
    return conv.data;
}

const addEpisodeToPlayedHistory = (conv, currentEpisode) => {
    var playedHistory = conv.data.playedHistory;
    if (typeof conv.data.playedHistory !== 'undefined' && conv.data.playedHistory.length > 0) {
        playedHistory = [...playedHistory, currentEpisode]
      } else {
        playedHistory = [currentEpisode];
      }
    conv.data.playedHistory = playedHistory;
    return conv.data;
}

module.exports = {
    getRemainingEpisodesId,
    addEpisodeToPlayedHistory,
    saveConvEpisodesData
}