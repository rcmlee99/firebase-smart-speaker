module.exports = (text) => {
    var podcastSSML = "";
    if (text === "Kochie’s Easy Steps to Financial Success Podcast" ) {
        podcastSSML = `Coshies Easy Steps to Financial Success Podcast`;
    } else if (text === "Kochie's Guide to Starting Your Own Business") {
        podcastSSML = `Coshies Guide to Starting Your Own Business`;
    } else if (text === "Puka Up with Wayne Schwass") {
        podcastSSML = `Pucker Up with Wayne Schwass`;
    } else {
        podcastSSML = text.escapeHTML();
    }
    return podcastSSML;
}