const getISOStringWithoutMillisec = (dateTime) => {
  let formattedDate = dateTime.toISOString().split('.')[0];
  formattedDate = `${formattedDate}Z`;
  return formattedDate;
};

module.exports={
  getISOStringWithoutMillisec
};
