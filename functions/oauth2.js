const functions = require("firebase-functions");
const { generateAuthCode, initialiseFirebaseAdmin} = require('./service/firebaseAdminService');

// Initialise FirebaseAdmin before using firebase functions
initialiseFirebaseAdmin();

exports.apiauthcode = functions.https.onRequest(async (req, res) => {
  console.log("request header",req.headers);
  console.log("request body",req.body);

  try {
    if (!req.headers.authorization || req.headers.authorization.indexOf('Basic ') === -1) {
        res.status(401).json({ message: 'Missing Authorization Header' });
    }
    // Using a simple Basic Auth Key for Server to Server Authentication. Do not require IAM at the moment.
    if (req.headers.authorization !== `Basic ${functions.config().webhook.apikey}`) {
        res.status(401).send('Authentication required.')
    }
    var response = await generateAuthCode(req.body);
    res.status(200).send(response);
  } catch (error) {
    console.error(error);
    res.status(400).send(error);
  }

});

exports.token = require('./access_token.js');
