const functions = require("firebase-functions");
const {getAccessTokenByRefreshToken, getAccessTokenByAuthCode, initialiseFirebaseAdmin} = require('./service/firebaseAdminService');

// Initialise FirebaseAdmin before using firebase functions
initialiseFirebaseAdmin();

module.exports = functions.https.onRequest(async (req, res) => {
  console.log("request header",req.headers);
  console.log("request body",req.body);

  if (!req.headers.authorization || req.headers.authorization.indexOf('Basic ') === -1) {
    return res.status(401).json({ message: 'Missing Authorization Header' });
  }

  let client_idSecret = Buffer.from(req.headers.authorization.split(" ")[1], 'base64').toString();
  let client_secret = client_idSecret.split(":")[1];
  if (client_secret !== functions.config().webhook.apikey) {
      return res.status(401).send('Authentication required.')
  }

  if (req.body.grant_type === "authorization_code") {
    return getAccessTokenByAuthCode(req, res);
  } else if (req.body.grant_type === "refresh_token") {
    return getAccessTokenByRefreshToken(req, res);
  } else {
    return res.send(404);
  }
});
