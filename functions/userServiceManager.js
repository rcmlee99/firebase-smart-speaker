const functions = require('firebase-functions');
const express = require('express');
const cors = require('cors');
const {getAllUsers,editUser, deleteUser, createUser, initialiseFirebaseAdmin} = require('./service/firebaseAdminService');
const config = require('./config/local');
const constants = require("./constants");
const portUserServiceManager = constants.portUserServiceManager;

// Initialise FirebaseAdmin before using firebase functions
initialiseFirebaseAdmin();

// Basic Auth = username: sca, password: local
const squidexKey = (process.env.NODE_ENV === 'local') ? config.squidex.secretkey : functions.config().squidex.secretkey;

// Automatically allow cross-origin requests
const app = express();
app.use(cors({ origin: true }));
app.use(express.json());

app.get('/', (req, res) => {
  res.status(200).send('Service OK');
});


app.post('/', async (req, res) => {
  if (!req.headers.authorization || req.headers.authorization.indexOf('Basic ') === -1) {
    return res.status(401).json({ message: 'Missing Authorization Header' });
  }

  if (req.headers.authorization !== `Basic ${squidexKey}`) {
    return res.status(401).json({ message: 'Authentication required.' });
  }

  if (req.headers.action==='get') {
    try {
      const limit = req.body.limit || undefined;
      const nextPageToken = req.body.nextPageToken || undefined;
      const allUsers = await getAllUsers(limit, nextPageToken);
      console.log('Successfully retrieved all users');
      return res.status(200).send(allUsers);
    } catch (e) {
      console.log('Get user action failed', e);
      return res.status(500).send(e);
    }
  }

  if (req.headers.action==='create') {
    try {
      const newUser = req.body.newuser;
      const createdUser = await createUser(newUser);
      console.log(`Successfully created user`);
      return res.status(200).send(createdUser);
    } catch (e) {
      console.log('Create user action failed', e);
      return res.status(500).send(e);
    }
  }

  if (req.headers.action==='edit') {
    try {
      const payload = req.body.payload || undefined;
      const uid = req.headers.uid || undefined;
      const editedUser = await editUser(uid, payload);
      console.log(`Successfully updated user's details`);
      return res.status(200).send(editedUser);
    } catch (e) {
      console.log('Edit user action failed', e);
      return res.status(500).send(e);
    }
  }

  if (req.headers.action==='delete') {
    try {
      const uid = req.headers.uid || undefined;
      await deleteUser(uid);
      console.log(`Successfully deleted user`);
      return res.status(200).send(JSON.stringify('Successfully deleted user'));
    } catch (e) {
      console.log('Delete user action failed', e);
      return res.status(500).send(e);
    }
  }
});

app.listen(portUserServiceManager);

const userServiceManagerHttp = functions.https.onRequest(app);
const userServiceManagerTest = app;

module.exports = {
  userServiceManagerHttp,
  userServiceManagerTest
};

