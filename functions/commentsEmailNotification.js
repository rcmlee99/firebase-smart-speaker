const functions = require('firebase-functions');
const config = require(`./config/test`);
const sgMail = require('@sendgrid/mail');

const EMAIL_SEND_TO = (process.env.NODE_ENV === 'local') ? config.sendgrid.emailto : functions.config().sendgrid.emailto;
const SENDGRID_API_KEY = (process.env.NODE_ENV === 'local') ? config.sendgrid.key :  functions.config().sendgrid.key;
const EMAIL_SEND_FROM = 'support@podcastOneAustralia.com';

sgMail.setApiKey(SENDGRID_API_KEY);

// Function to send email.
const sendMail = async (msg) => {
  try {
    await sgMail.send(msg);
    console.log('email sent!')
  } catch (e) {
    console.log('error', e)
  }
};

 // Executed when a new comment is created
 const newComment = functions.firestore
    .document('episodes/{episodesId}/comments/{commentId}')
    .onCreate((snapshot) => {

      const comment = snapshot.data();

      const msg = {
        to: EMAIL_SEND_TO,
        from: EMAIL_SEND_FROM,
        subject:  `New comment for ${comment.showTitle}`,
        templateId: 'd-c3b01d5804114a179be4934fbf3c0236',
        dynamic_template_data: {
          show: comment.showTitle,
          message: comment.message,
          author: comment.authorName || '',
          authorEmail: comment.authorEmail || '',
        }
      };
      return sendMail(msg);
    });

// Executed when a comment receives a complaint.
const reportedComment = functions.firestore
    .document('episodes/{episodesId}/comments/{commentId}')
    .onWrite((change) => {

      const previousData = change.before.data();
      const newData = change.after.data();

      if (newData.modTotal > previousData.modTotal) {
        const msg = {
          to: EMAIL_SEND_TO,
          from: EMAIL_SEND_FROM,
          subject:  `New compliant of a comment from ${newData.showTitle}`,
          templateId: 'd-8ab7d38a153f4f859ad6f43c9eb9bfcf',
          dynamic_template_data: {
            show: newData.showTitle,
            message: newData.message,
            author: newData.authorName || '',
            authorEmail: newData.authorEmail || '',
            modExplicitCount: newData.modExplicitCount,
            modHarassmentCount:newData.modHarassmentCount,
            modHateSpeechCount: newData.modHateSpeechCount,
            modSpamCount: newData.modSpamCount,
          }
        };
        return sendMail(msg);
      }
    });

module.exports ={
  newComment,
  reportedComment,
};
