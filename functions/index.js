const { dialogflowhttp } = require('./dialogflowFirebaseFulfillment.js');
const { apisynthetic } = require('./apisynthetic.js');

const { apinewpodcasthttp } = require('./apinewpodcast.js');
const { apifirestorehttp } = require('./apifirestore.js');

const { newComment, reportedComment } = require('./commentsEmailNotification.js');

const { userServiceManagerHttp } = require('./userServiceManager');

const { activationEmail, resendVerifyEmail } = require('./auth-email/activation');

const { resetEmail } = require('./auth-email/reset');

// Old Access Token Endpoint
exports.access_token = require('./access_token.js');
// New Oauth Endpoint follow OIDC format
exports.oauth2 = require('./oauth2.js');

// Smartspeaker endpoints
exports.dialogflowFirebaseFulfillment = dialogflowhttp;
exports.apisynthetic = apisynthetic;
exports.apinewpodcast = apinewpodcasthttp;
exports.apifirestore = apifirestorehttp;

// Web service
exports.userservicemanager = userServiceManagerHttp;
exports.commentsNewNotification = newComment;
exports.commentsReportedNotification = reportedComment;

// Auth email service
exports.activationEmail = activationEmail;
exports.apiverifyemail = resendVerifyEmail;
exports.resetEmail = resetEmail;

