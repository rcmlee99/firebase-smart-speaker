// See https://github.com/dialogflow/dialogflow-fulfillment-nodejs
// for Dialogflow fulfillment library docs, samples, and to report issues
'use strict';

const functions = require('firebase-functions');
const { dialogflow, Suggestions, SimpleResponse } = require('actions-on-google')
const { getPodcastEpisodes, getPodcastEpisodesById, getEpisodeDetail, getRecommendedList } = require('./service/graphqlService');
const { addOrUpdateUserEpisode, addOrUpdateUserFavouritePodcast} = require('./service/firebaseAdminService');
const { getCurrentUser, getUserLastEpisode, getUserFavShows } = require('./service/firebaseService');
const { episodeMediaObject, podcastCarouselObject, podcastOneObject, signInCardObject, sendConvObject, exitCardObject, splashCardObject } = require('./model/googleHomeObject');
const { getRemainingEpisodesId, saveConvEpisodesData, addEpisodeToPlayedHistory } = require('./model/conversationObject');
const convertToSSML = require('./model/ssmlObject');
var recommendedList = require('./data/recommendedList.json');
const audioLink = "https://ghome-oauth-link.firebaseapp.com/sound/podcastone.mp3";
const helpText = `Here are a few things you can ask me to do. Ask me to play a podcast by saying its name or say 'popular' to hear the most popular podcasts.`;

// Initialize Dialogflow thru Actions-On-Google
const app = dialogflow({
  debug: true,
})

const dialogflowhttp = functions.https.onRequest(app);
const dialogflowtest = app;
const podcastListLimit = 3;
const favShowListLimit = 2;
var resp, mediaresp, suggestion, respConvObjects;

// More build in intents https://developers.google.com/actions/reference/rest/intents
app.intent('Default Welcome Intent' || 'actions.intent.MAIN', async conv => {
  console.log("welcome :::", JSON.stringify(conv.body.queryResult));
  console.log("User :::", JSON.stringify(conv.user));
  let accessToken = conv.user.access.token;
  var lastEpisode, favShows;

  if (accessToken) {
    let firebaseUser = await getCurrentUser(accessToken);
    console.log("firebaseUser :::", JSON.stringify(firebaseUser));
    lastEpisode = await getUserLastEpisode(firebaseUser);
    favShows = await getUserFavShows(firebaseUser);
    conv.data.firebaseuser = firebaseUser;
    conv.data.lastEpisodeId = lastEpisode.episodeId;
    conv.data.favShows = favShows;
  }

  if (lastEpisode) {
    let currentEpisode = await getEpisodeDetail(lastEpisode.episodeId);
    resp = `<speak><audio src="${audioLink}"></audio>Would you like to play your last episode of ${convertToSSML(currentEpisode.episode.podcast.name)}?</speak>`;
    mediaresp = podcastOneObject(currentEpisode.episode);
    suggestion = ["Yes", "No"];
    respConvObjects = [resp, mediaresp, suggestion];
  } else if (favShows && favShows.length>0) {
    // If there is at least one favourite show, find the podcast and display them
    respConvObjects = await favShowPodcast(conv);
  } else {
    // If no last episode or favourite show. then show recommended podcat
    respConvObjects = await standardRecommendPodcast(conv);
  }
  sendConvObject(conv, ...respConvObjects);
})

async function favShowPodcast(conv) {
  console.log("favShowPodcast :::", JSON.stringify(conv.data));
  const podcastSuggestion = [];
  const favShowsObjects = []
  var podcastSSML = "";
  const favShows = conv.data.favShows;

  if (favShows.length===1) {
    let podcastQuery = await getPodcastEpisodesById(favShows[0]);
    podcastSuggestion.push(podcastQuery.podcast.name.truncate());
    let currentEpisode = await getEpisodeDetail(podcastQuery.podcast.episodes.items[0].id);
    mediaresp = podcastOneObject(currentEpisode.episode);
  } else {
    await asyncForEach(favShows, async (podcastId, index) => {
      if (index>=favShowListLimit) return;
      let podcastQuery = await getPodcastEpisodesById(podcastId);
      let podcast = podcastQuery.podcast;
      podcastSuggestion.push(podcast.name.truncate());
      podcastSSML = podcastSSML + `<s>${convertToSSML(podcast.name)}</s><break time="0.5s"/>`;
      favShowsObjects.push(podcast);
    });
    mediaresp = podcastCarouselObject(favShowsObjects, favShowListLimit);
  }
  if (conv.surface.capabilities.has('actions.capability.SCREEN_OUTPUT')) {
    resp = `<speak>Here are your favourite podcasts.</speak>`;
  } else {
    resp = `<speak>Just say the name of one of your favourite podcasts to start listening<p>${podcastSSML}.</p> What would you like to hear?</speak>`;
  }
  return [resp, mediaresp, podcastSuggestion];
}

async function standardRecommendPodcast(conv) {
  console.log("standardRecommendPodcast :::", JSON.stringify(conv.data));
  const podcastSuggestion = [];
  var podcastSSML = "";
  recommendedList = await getRecommendedList();
  recommendedList.popularPodcasts.forEach((podcast, index) => {
    if (index>=podcastListLimit) return;
    podcastSuggestion.push(podcast.name.truncate());
    podcastSSML = podcastSSML + `<s>${convertToSSML(podcast.name)}</s><break time="0.5s"/>`;
  })
  mediaresp = podcastCarouselObject(recommendedList.popularPodcasts, podcastListLimit);
  if (conv.surface.capabilities.has('actions.capability.SCREEN_OUTPUT')) {
    resp = `<speak>Here are some popular podcasts.</speak>`;
  } else {
    resp = `<speak>Just say the name of one of these popular podcasts to start listening<p>${podcastSSML}.</p> What would you like to hear?</speak>`;
  }
  return [resp, mediaresp, podcastSuggestion];
}

app.intent('Default Welcome Intent - yes', async conv => {
  console.log("Default Welcome Intent - yes :::", JSON.stringify(conv.request));
  let currentEpisode = await getEpisodeDetail(conv.data.lastEpisodeId);
  console.log("currentEpisode :::", JSON.stringify(currentEpisode));
  let podcastQuery = await getPodcastEpisodes(currentEpisode.episode.podcast.slug);
  var allEpisodesId = podcastQuery.podcast.episodes.items;
  var podcastName = podcastQuery.podcast.name;
  var remainingEpisodes = getRemainingEpisodesId(allEpisodesId);
  conv.data.podcastName = podcastName;
  saveConvEpisodesData(conv, [], currentEpisode.episode.id, remainingEpisodes);
  if (conv.surface.capabilities.has('actions.capability.MEDIA_RESPONSE_AUDIO') && currentEpisode !== undefined) {
    conv.ask(new SimpleResponse({
      speech: `<speak>Playing the last episode of ${convertToSSML(podcastName)} on Podcast One Australia</speak>`,
      text: `Playing the last episode of ${podcastName} on Podcast One Australia`
    }));
    console.log(`playedEpisode :::`, JSON.stringify(currentEpisode.episode));
    addEpisodeToPlayedHistory(conv, currentEpisode.episode.id);
    if (conv.data.firebaseuser) {
      const payload = {
          "user" : conv.data.firebaseuser,
          "episode" : currentEpisode.episode
      }
      addOrUpdateUserEpisode(payload);
    }
    conv.close(episodeMediaObject(currentEpisode.episode));
    if (remainingEpisodes && remainingEpisodes.length) {
      conv.ask(new Suggestions([`Next episode`]));
    }
  }
})

app.intent('Default Welcome Intent - no', async conv => {
  console.log("Default Welcome Intent - no :::", JSON.stringify(conv.request));
  if (conv.data.favShows && conv.data.favShows.length>0) {
    // If there is at least one favourite show, find the podcast and display them
    respConvObjects = await favShowPodcast(conv);
  } else {
    // If no last episode or favourite show. then show recommended podcat
    respConvObjects = await standardRecommendPodcast(conv);
  }
  sendConvObject(conv, ...respConvObjects);
})

app.intent('Cancel' || 'actions.intent.CANCEL', conv => {
  console.log("cancel :::", JSON.stringify(conv.request));
  console.log(`queryTextCancel=${JSON.stringify(conv.body.queryResult.queryText)}, Confidence=${conv.body.queryResult.intentDetectionConfidence}`);
  conv.close(new SimpleResponse({
    speech: `<speak><audio src="${audioLink}"></audio></speak>`,
    text: `Thanks for using Podcast One Australia`
  }));
})

app.intent('Carousel Options', async (conv, input, arg) => {
  console.log("carousel :::", JSON.stringify(conv.body.queryResult));
  console.log(`queryTextPlay=${JSON.stringify(conv.body.queryResult.queryText)}, Confidence=${conv.body.queryResult.intentDetectionConfidence}`);
  console.log("arg :::", JSON.stringify(arg));
  console.log("conv data :::", JSON.stringify(conv.data));
  const queryResult = conv.body.queryResult;
  var resp = `Which podcast would you like to play?`;
  if (arg && queryResult.allRequiredParamsPresent) {
    let podcastQuery = await getPodcastEpisodes(arg);
    var podcastName = podcastQuery.podcast.name;
    var allEpisodesId = podcastQuery.podcast.episodes.items;
    let currentEpisode = await getEpisodeDetail(allEpisodesId.shift().id);
    console.log("currentEpisode :::", JSON.stringify(currentEpisode));
    var remainingEpisodes = getRemainingEpisodesId(allEpisodesId);
    conv.data.podcastName = podcastName;
    saveConvEpisodesData(conv, [], currentEpisode.episode.id, remainingEpisodes);

    if (conv.surface.capabilities.has('actions.capability.MEDIA_RESPONSE_AUDIO') && currentEpisode!==undefined ) {
      conv.ask(new SimpleResponse({
        speech: `<speak>Playing the latest episode of ${convertToSSML(podcastName)} on Podcast One Australia</speak>`,
        text: `Playing the latest episode of ${podcastName} on Podcast One Australia`
      }));
      console.log(`playedEpisode :::`, JSON.stringify(currentEpisode.episode));
      addEpisodeToPlayedHistory(conv, currentEpisode.episode.id);
      if (conv.data.firebaseuser) {
        const payload = {
            "user" : conv.data.firebaseuser,
            "episode" : currentEpisode.episode,
            "podcastId" : podcastQuery.podcast.id
        }
        addOrUpdateUserEpisode(payload);
        addOrUpdateUserFavouritePodcast(payload);
      }
      conv.close(episodeMediaObject(currentEpisode.episode));
      if (remainingEpisodes && remainingEpisodes.length) {
        conv.ask(new Suggestions([`Next episode`]));
      }
    }
  } else {
    conv.ask(resp);
  }
})

app.intent('Recommend', async conv => {
  console.error("Recommend :::", JSON.stringify(conv.request));
  console.log(`queryTextRecommend=${JSON.stringify(conv.body.queryResult.queryText)}, Confidence=${conv.body.queryResult.intentDetectionConfidence}`);
  const respConvObjects = await standardRecommendPodcast(conv);
  sendConvObject(conv, ...respConvObjects);
})

app.intent('Default Fallback Intent', async conv => {
  console.error("Fallback :::", JSON.stringify(conv.request));
  console.log(`queryTextFallback=${JSON.stringify(conv.body.queryResult.queryText)}, Confidence=${conv.body.queryResult.intentDetectionConfidence}`);
  conv.ask(helpText);
})

app.intent('actions.intent.CONFIRMATION', conv => {
  console.log("confirmation :::", JSON.stringify(conv.request));
  conv.close('Confirmation');
})

app.intent('actions.intent.PERMISSION', conv => {
  console.log("permission :::", JSON.stringify(conv.request));
  conv.close('PERMISSION');
})

app.intent('Next podcast', async (conv, input, arg) => {
  console.log("nextPodcast :::", JSON.stringify(conv.request));
  console.log(`queryTextPlay=${JSON.stringify(conv.body.queryResult.queryText)}, Confidence=${conv.body.queryResult.intentDetectionConfidence}`);
  console.log("input :::", JSON.stringify(input));
  console.log("arg :::", JSON.stringify(arg));
  console.log("conv data :::", JSON.stringify(conv.data));
  const mediaStatus = conv.arguments.get('MEDIA_STATUS');
  if (mediaStatus && mediaStatus.status === 'STATUS_UNSPECIFIED') {
    return;
  } else if (mediaStatus && mediaStatus.status === 'FAILED') {
    conv.ask("Failed to play media");
    return;
  }

  var previousEpisodes = [];
  if (typeof conv.data.previousEpisodes !== 'undefined' && conv.data.previousEpisodes.length > 0) {
    previousEpisodes = [...conv.data.previousEpisodes, conv.data.currentEpisode];
  } else {
    previousEpisodes = [conv.data.currentEpisode];
  }
  var remainingEpisodes = conv.data.remainingEpisodes;
  var currentEpisodeId = remainingEpisodes.shift();
  // If currentEpisode has been played before, then goto next remaining episode
  while (conv.data.playedHistory.some(e => e === currentEpisodeId))  {
    currentEpisodeId = remainingEpisodes.shift();
  }
  let currentEpisode = await getEpisodeDetail(currentEpisodeId);

  console.log("currentEpisode :::", JSON.stringify(currentEpisode));
  saveConvEpisodesData(conv, previousEpisodes, currentEpisode.episode.id, remainingEpisodes);

  if (conv.surface.capabilities.has('actions.capability.MEDIA_RESPONSE_AUDIO') && currentEpisode!==undefined ) {
    conv.ask(new SimpleResponse({
      speech: `<speak>Playing the next episode of ${convertToSSML(conv.data.podcastName)} on Podcast One Australia</speak>`,
      text: `Playing the next episode of ${conv.data.podcastName} on Podcast One Australia`
    }));
    console.log(`playedEpisode :::`, JSON.stringify(currentEpisode.episode));
    addEpisodeToPlayedHistory(conv, currentEpisode.episode.id);
    if (conv.data.firebaseuser) {
      const payload = {
          "user" : conv.data.firebaseuser,
          "episode" : currentEpisode.episode,
      }
      addOrUpdateUserEpisode(payload);
    }
    conv.close(episodeMediaObject(currentEpisode.episode));
    if (remainingEpisodes && remainingEpisodes.length) {
      conv.ask(new Suggestions([`Next episode`]));
    }
    // if (previousEpisodes && previousEpisodes.length) {
    //   conv.ask(new Suggestions([`Previous episode`]));
    // }
  } else {
    if (mediaStatus && mediaStatus.status === 'FINISHED') {
      conv.ask(new SimpleResponse({
        speech: `<speak>There are no unplayed episodes of ${convertToSSML(conv.data.podcastName)} please choose a different podcast</speak>`,
        text: `There are no unplayed episodes of ${conv.data.podcastName} please choose a different podcast`
      }));
    }
  }
})

app.intent('Previous podcast', async (conv, input, arg) => {
  console.log("previousPodcast :::", JSON.stringify(conv.request));
  console.log(`queryTextPlay=${JSON.stringify(conv.body.queryResult.queryText)}, Confidence=${conv.body.queryResult.intentDetectionConfidence}`);
  var remainingEpisodes = [];
  if (typeof conv.data.remainingEpisodes !== 'undefined' && conv.data.remainingEpisodes.length > 0) {
    remainingEpisodes = [conv.data.currentEpisode, ...conv.data.remainingEpisodes];
  } else {
    remainingEpisodes = [conv.data.currentEpisode];
  }
  var previousEpisodes = conv.data.previousEpisodes;
  let currentEpisode = await getEpisodeDetail(previousEpisodes.pop());
  console.log("currentEpisode :::", JSON.stringify(currentEpisode));
  saveConvEpisodesData(conv, previousEpisodes, currentEpisode.episode.id, remainingEpisodes);
  const mediaStatus = conv.arguments.get('MEDIA_STATUS');

  if (conv.surface.capabilities.has('actions.capability.MEDIA_RESPONSE_AUDIO') && currentEpisode!==undefined ) {
    conv.ask(new SimpleResponse({
      speech: `<speak>Playing the previous episode of ${convertToSSML(conv.data.podcastName)} on Podcast One Australia</speak>`,
      text: `Playing the next previous of ${conv.data.podcastName} on Podcast One Australia`
    }));
    console.log(`playedEpisode :::`, JSON.stringify(currentEpisode.episode));
    addEpisodeToPlayedHistory(conv, currentEpisode.episode.id);
    conv.close(episodeMediaObject(currentEpisode.episode));
    if (remainingEpisodes && remainingEpisodes.length) {
      conv.ask(new Suggestions([`Next episode`]));
    }
    // if (previousEpisodes && previousEpisodes.length) {
    //   conv.ask(new Suggestions([`Previous episode`]));
    // }
  } else {
    if (mediaStatus && mediaStatus.status === 'FINISHED') {
      conv.ask("No more episodes available.");
    }
  }
})

app.intent('Resume podcast', async (conv, input, arg) => {
  console.log("resumePodcast :::", JSON.stringify(conv.request));
  console.log(`queryTextPlay=${JSON.stringify(conv.body.queryResult.queryText)}, Confidence=${conv.body.queryResult.intentDetectionConfidence}`);
  let accessToken = conv.user.access.token;
  let currentEpisodeId = conv.data.currentEpisode;

  if (accessToken === undefined) {
      conv.ask(signInCardObject());
  }

  if (currentEpisodeId) {
      let currentEpisode = await getEpisodeDetail(currentEpisodeId);
      console.log("currentEpisode :::", JSON.stringify(currentEpisode));

      //Build remaining playlist
      let podcastQuery = await getPodcastEpisodes(currentEpisode.episode.podcast.slug);
      var podcastName = podcastQuery.podcast.name;
      var allEpisodesId = podcastQuery.podcast.episodes.items;
      var remainingEpisodes = getRemainingEpisodesId(allEpisodesId);
      conv.data.podcastName = podcastName;
      saveConvEpisodesData(conv, [], currentEpisode.episode.id, remainingEpisodes);

      if (conv.surface.capabilities.has('actions.capability.MEDIA_RESPONSE_AUDIO') && currentEpisode!==undefined ) {
        conv.ask(new SimpleResponse({
          speech: `<speak>This is Podcast One Australia. Resuming ${convertToSSML(podcastName)} </speak>`,
          text: `This is Podcast One Australia. Resuming ${podcastName}`
        }));
        addEpisodeToPlayedHistory(conv, currentEpisode.episode.id);
        conv.close(episodeMediaObject(currentEpisode.episode));
        if (remainingEpisodes && remainingEpisodes.length) {
          conv.ask(new Suggestions([`Next episode`]));
        }
        return;
      }
  } else {
    conv.ask(new SimpleResponse({
      speech: `<speak>There are no unplayed episodes, please choose a different podcast</speak>`,
      text: `There are no unplayed episodes, please choose a different podcast`
    }));
  }
})

app.intent('Play podcast', async (conv, input, arg) => {
  console.log("playPodcast :::", JSON.stringify(conv.request));
  await playPodcast(conv, input);
})

app.intent('Play Implicit', async (conv, input, arg) => {
  console.log("playImplicit :::", JSON.stringify(conv.request));
  await playPodcast(conv, input);
})

async function playPodcast(conv, input) {
  console.log(`queryTextPlay=${JSON.stringify(conv.body.queryResult.queryText)}, Confidence=${conv.body.queryResult.intentDetectionConfidence}`);
  console.log("input :::", JSON.stringify(input));
  console.log("conv data :::", JSON.stringify(conv.data));
  const queryResult = conv.body.queryResult;
  if (input.podcast && queryResult.allRequiredParamsPresent) {
    let podcastQuery = await getPodcastEpisodes(input.podcast);
    var podcastName = podcastQuery.podcast.name;
    var allEpisodesId = podcastQuery.podcast.episodes.items;
    let currentEpisode = await getEpisodeDetail(allEpisodesId.shift().id);
    console.log("currentEpisode :::", JSON.stringify(currentEpisode));
    var remainingEpisodes = getRemainingEpisodesId(allEpisodesId);
    conv.data.podcastName = podcastName;
    saveConvEpisodesData(conv, [], currentEpisode.episode.id, remainingEpisodes);
    if (conv.surface.capabilities.has('actions.capability.MEDIA_RESPONSE_AUDIO') && currentEpisode !== undefined) {
      conv.ask(new SimpleResponse({
        speech: `<speak>Playing the latest episode of ${convertToSSML(podcastName)} on Podcast One Australia</speak>`,
        text: `Playing the latest episode of ${podcastName} on Podcast One Australia`
      }));
      console.log(`playedEpisode :::`, JSON.stringify(currentEpisode.episode));
      addEpisodeToPlayedHistory(conv, currentEpisode.episode.id);
      if (conv.data.firebaseuser) {
        const payload = {
            "user" : conv.data.firebaseuser,
            "episode" : currentEpisode.episode,
            "podcastId" : podcastQuery.podcast.id
        }
        addOrUpdateUserEpisode(payload);
        addOrUpdateUserFavouritePodcast(payload);
      }
      conv.close(episodeMediaObject(currentEpisode.episode));
      if (remainingEpisodes && remainingEpisodes.length) {
        conv.ask(new Suggestions([`Next episode`]));
      }
    }
  }
  else {
    conv.ask(`Which podcast would you like to play?`);
  }
}

app.intent('Help', (conv) => {
  console.log("convQuery :::", JSON.stringify(conv.query));
  console.log("convArg :::", JSON.stringify(conv.arguments));
  conv.ask(helpText);
});

app.intent('Unrecognized Deep Link Fallback', (conv) => {
  console.log("convQuery :::", JSON.stringify(conv.query));
  console.log("convArg :::", JSON.stringify(conv.arguments));
  conv.ask(helpText);
});

app.catch((conv,error) => {
  console.error("error :::", JSON.stringify(error));
  conv.close(`Something went wrong, please try again later.`);
})

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}
String.prototype.escapeHTML = function() {
        return this.replace(/&/g, "&amp;")
                   .replace(/</g, "&lt;")
                   .replace(/>/g, "&gt;")
                   .replace(/"/g, "&quot;")
                   .replace(/'/g, "&#039;");
}

String.prototype.truncate = function() {
  const n = 25 // Length of String to truncate
  if (this.length <= n) { return this; }
  return this.substr(0, n-3) + "...";
};

module.exports = {
  dialogflowhttp,
  dialogflowtest
};
