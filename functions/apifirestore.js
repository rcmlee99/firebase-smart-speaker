/**
 * Contains API to update new podcast changes to Dialogflow entities and alexa slots
 *
 * For full API reference, please refer to
 * https://confluence.sca.com.au/display/SS/API+-+New+Podcast
 *
 */

'use strict';

const functions = require('firebase-functions');
const {
    addOrUpdateUserEpisode,
    addOrUpdateUserFavouritePodcast,
    getUserLastEpisode,
    getUserFavShows,
    initialiseFirebaseAdmin
} = require('./service/firebaseAdminService');
const constants = require("./constants");
const portAPIFirestore = constants.portAPIFirestore;

const { mockFirestoreData } = require('./test/static/mockData');

// Initialise FirebaseAdmin before using firebase functions
initialiseFirebaseAdmin();


const express = require('express');
const cors = require('cors');
const app = express();
const API_PREFIX = 'v1/api/firestore';

// Automatically allow cross-origin requests
app.use(cors({ origin: true }));
app.use(express.json());
app.use((req, res, next) => {
    if (req.url.indexOf(`/${API_PREFIX}`) === 0) {
        req.url = req.url.substring(API_PREFIX.length + 1) + '/';
    }
    next();
});

app.get('/', (req, res) => {
    res.status(200).send('Service OK');
});

app.post('/', async (req, res) => {
    console.log(req.body);
    try {
        if (!req.headers.authorization || req.headers.authorization.indexOf('Basic ') === -1) {
            res.status(401).json({ message: 'Missing Authorization Header' });
        }

        // Using a simple Basic Auth Key for Server to Server Authentication. Do not require IAM at the moment.
        if (req.headers.authorization !== `Basic ${functions.config().webhook.apikey}`) {
            res.status(401).send('Authentication required.')
        }

        var response;
        if (req.body.action==='getUserFavShows') {
            response = await getUserFavShows(req.body);
        } else if (req.body.action==='getUserLastEpisode') {
            response = await getUserLastEpisode(req.body);
        } else if (req.body.action==='addOrUpdateUserEpisode') {
            response = await addOrUpdateUserEpisode(req.body);
        } else if (req.body.action==='addOrUpdateUserFavouritePodcast') {
            response = await addOrUpdateUserFavouritePodcast(req.body);
        }
        res.status(200).send(response);
    } catch (error) {
        console.error(error);
        res.status(400).send();
    }
});

app.listen(portAPIFirestore);

const apifirestorehttp = functions.https.onRequest(app);
const apifirestoretest = app;

module.exports = {
    apifirestorehttp,
    apifirestoretest
};

