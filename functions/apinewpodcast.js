/**
 * Contains API to update new podcast changes to Dialogflow entities and alexa slots
 *
 * For full API reference, please refer to
 * https://confluence.sca.com.au/display/SS/API+-+New+Podcast
 *
 */

'use strict';
const { updateBucket, getBucket, initialiseFirebaseAdmin } = require('./service/firebaseAdminService');
const { findEntities, updateEntities, deleteEntities } = require('./service/dialogflowService');
const { updateSlots, deleteSlots } = require('./service/smapiService');
const functions = require('firebase-functions');
const env = process.env.NODE_ENV || 'local';
const config = require(`./config/${env}.json`);
const constants = require("./constants");
const portAPINewPodcast = constants.portAPINewPodcast;

// Initialise FirebaseAdmin before using firebase functions
initialiseFirebaseAdmin();

let sendgridConfig = config.sendgrid;

const EMAIL_SEND_FROM = 'support@podcastOneAustralia.com';
var projectID = 'test-podcastone';
if (process.env.NODE_ENV === 'production') {
    sendgridConfig = functions.config().sendgrid;
    projectID = functions.config().fb.projectid;
}

const newEntity = (jsonData) => {
    return {
        "value": jsonData.slug,
        "synonyms": [
            jsonData.name
        ]
    }
};

const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(sendgridConfig.key);

// Function to send email.
const sendMail = async (jsonData, result) => {
    const contentText = `Request from webhook: ${JSON.stringify(jsonData)} \n\nResponse API : ${JSON.stringify(result)} `
    const msg = {
        to: sendgridConfig.newpodcast,
        from: EMAIL_SEND_FROM,
        subject:  `Project ${projectID} - New podcast slug ${jsonData.slug}`,
        text: contentText
    };

    try {
        await sgMail.send(msg);
        console.log('email sent!')
    } catch (e) {
        console.log('error', e)
    }
};

const mockData = { name: 'This is just a test', slug: 'hamish-test13'};
const mockEntities = require('./data/dialogflowAgent/entities/podcast_entries_en.json');
const filename = 'podcast_entries_en.json';

const apinewpodcastAddorUpdate = async (jsonData) => {

    // Load current Entities from Cloud Storage / MockEntities
    const entitiesData = await getBucket(filename);

    // Check if new Podcast exist in Entities or not
    const resultFindEntity = entitiesData.filter((item) => {
        return item.value === jsonData.slug;
    })
    if (resultFindEntity.length) {
        console.log(`Found entity slug ${jsonData.slug}`);
        // No need to do anything
        return ("No entity need to be updated");
    } else {
        console.log(`Not Found entity slug ${jsonData.slug}`)
        entitiesData.push(newEntity(jsonData));

        // Find the entity type Podcast
        const resultFindEntities = await findEntities('podcast');
        // Update the Dialogflow Entity Model
        const resultUpdateEntities = await updateEntities(resultFindEntities, entitiesData, jsonData.slug);
        // Update the Alexa Slot Model
        const resultUpdateSlots = await updateSlots(jsonData);

        return new Promise((resolve, reject) => {
            Promise.all([resultUpdateEntities, resultUpdateSlots]).then(() => {
                updateBucket(filename, resultUpdateEntities[0].entities, jsonData);
            }).then(() => {
                resolve(`Entity slug ${jsonData.slug} updated in Dialogflow and Alexa`);
            }).catch((err) => {
                console.error('Error: ', err);
                reject(`Error: ${err}`);
            });
        })
    }
};

const apinewpodcastDelete = async (jsonData) => {

    // Load current Entities from Cloud Storage / MockEntities
    const entitiesData = await getBucket(filename);
    const resultEntities = entitiesData.filter((item) => {
        return item.value !== jsonData.slug;
    })

    // Find the entity type Podcast
    const resultFindEntities = await findEntities('podcast');
    // Check if new Podcast exist in Entities or not
    const resultFindEntity = resultFindEntities.entities.filter((item) => {
        return item.value === jsonData.slug;
    })
    if (resultFindEntity.length) {

        // Delete the Dialogflow Entity
        const resultDeleteEntities = await deleteEntities(resultFindEntities.name, jsonData.slug);

        // Delete the Alexa Slot
        const resultUpdateSlots = await deleteSlots(jsonData);

        return new Promise((resolve, reject) => {
            Promise.all([resultDeleteEntities, resultUpdateSlots]).then(() => {
                updateBucket(filename, resultEntities, jsonData);
            }).then(() => {
                resolve(`Entity slug ${jsonData.slug} removed in Dialogflow and Alexa`);
            }).catch((err) => {
                console.error('Error: ', err);
                reject(`Error: ${err}`);
            });
        });

    } else {
        console.log(`No entity slug ${jsonData.slug}`);
        // No need to do anything
        return ("No entity need to be updated");
    }

}

const express = require('express');
const cors = require('cors');
const app = express();
const API_PREFIX = 'v1/api/podcast';

// Automatically allow cross-origin requests
app.use(cors({ origin: true }));
app.use(express.json());
app.use((req, res, next) => {
    if (req.url.indexOf(`/${API_PREFIX}`) === 0) {
        req.url = req.url.substring(API_PREFIX.length + 1) + '/';
    }
    next();
});

app.get('/', (req, res) => {
    res.status(200).send('Service OK');
});

app.post('/', (req, res) => {
    console.log(req.body);
    if (!req.headers.authorization || req.headers.authorization.indexOf('Basic ') === -1) {
        return res.status(401).json({ message: 'Missing Authorization Header' });
    }

    // Using a simple Basic Auth Key for Server to Server Authentication. Do not require IAM at the moment.
    if (req.headers.authorization !== `Basic ${functions.config().webhook.apikey}`) {
        return res.status(401).send('Authentication required.')
    }

    if (req.body.action==='add') {
        apinewpodcastAddorUpdate(req.body).then((result) => {
            res.status(200).send(result);
            if (process.env.NODE_ENV === 'production' && result!=='No entity need to be updated') {
                sendMail(req.body, result);
            }
        }).catch((err) => {
            res.status(400).send(err);
            if (process.env.NODE_ENV === 'production') {
                sendMail(req.body, err);
            }
        });
    } else if (req.body.action==='delete') {
        apinewpodcastDelete(req.body).then((result) => {
            res.status(200).send(result);
            if (process.env.NODE_ENV === 'production' && result!=='No entity need to be updated') {
                sendMail(req.body, result);
            }
        }).catch((err) => {
            res.status(400).send(err);
            if (process.env.NODE_ENV === 'production') {
                sendMail(req.body, err);
            }
        });
    }

});

app.listen(portAPINewPodcast);

const apinewpodcasthttp = functions.https.onRequest(app);
const apinewpodcasttest = app;

module.exports = {
    apinewpodcasthttp,
    apinewpodcasttest
};

