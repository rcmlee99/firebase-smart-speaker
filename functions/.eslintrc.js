module.exports = {
    "env": {
        "commonjs": true,
        "es6": true,
        "node": true, 
        "mocha": true
    },
    "extends": [
        "eslint:recommended"
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 2018
    },
    "rules": {
        "no-console": "off",
        "no-unused-vars": "off",
        "no-prototype-builtins": "error",
        "no-unreachable": "error",
        "comma-dangle": [
            "error",
            {
                "arrays": "ignore",
                "objects": "ignore",
                "imports": "always-multiline",
                "exports": "always-multiline",
                "functions": "ignore"
            }
        ],
        "object-curly-newline": "off",
        "function-paren-newline": "off",
        "eqeqeq": ["error", "smart"],
        "space-before-blocks": "error",
        "keyword-spacing": [
            "error", 
            { 
                "before": true,
                "after": true  
            }
        ],
        "arrow-spacing": [
            "error", 
            { 
                "before": true,
                "after": true  
            }
        ]
    }
};