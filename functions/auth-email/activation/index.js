const functions = require('firebase-functions');
const { initialiseFirebaseAdmin, admin } = require('../../service/firebaseAdminService');
const { sendMail } = require('../../service/sendMailService');

initialiseFirebaseAdmin();

const createMail = (email, firebaseLink) => {
  const msg = {
    to: email,
    from: 'noreply@podcastone-australia.firebaseapp.com',
    templateId: 'd-e73a4685aba2436eb691a6b0162e3747',
    dynamic_template_data: {
      link: encodeURI(firebaseLink)
    },
  };
  return msg;
};

const createLinkSendEmail = async (email) => {
  const firebaseLink = await admin.auth().generateEmailVerificationLink(email);
  const msg = createMail(email, firebaseLink);
  return await sendMail(msg);
};

// Resend Verify Email via Admin API
const resendVerifyEmail = functions.https.onRequest(async (req, res) => {
  console.log("request header",req.headers);
  console.log("request body",req.body);
  try {
      if (!req.headers.authorization || req.headers.authorization.indexOf('Basic ') === -1) {
          res.status(401).json({ message: 'Missing Authorization Header' });
      }
      // Using a simple Basic Auth Key for Server to Server Authentication. Do not require IAM at the moment.
      if (req.headers.authorization !== `Basic ${functions.config().webhook.apikey}`) {
          res.status(401).send('Authentication required.')
      }
      var response = await createLinkSendEmail(req.body.email);
      res.status(200).send(response);
  } catch (error) {
      console.error(error);
      res.status(400).send();
  }
});

const activationEmail = functions.https.onCall(async data => {
  const { email } = data;
  return await createLinkSendEmail(email)
});

module.exports = {
  activationEmail,
  resendVerifyEmail
};