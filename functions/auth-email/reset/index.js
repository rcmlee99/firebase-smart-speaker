const functions = require('firebase-functions');
const admin = require('firebase-admin');
const { sendMail } = require('../../service/sendMailService');

exports.resetEmail = functions.https.onCall(async data => {
  const createMail = (email, firebaseLink) => {
    const msg = {
      to: email,
      from: 'noreply@podcastone-australia.firebaseapp.com',
      templateId: 'd-3f980c21be8643eabab78508b444fb06',
      dynamic_template_data: {
        link: encodeURI(firebaseLink)
      },
    };
    return msg;
  };

  const { email } = data;
  const firebaseLink = await admin.auth().generatePasswordResetLink(email);
  const msg = createMail(email, firebaseLink);
  return sendMail(msg);
});
